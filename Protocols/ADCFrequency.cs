﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UPDConsole.Protocols
{
    internal class ADCFrequency : Response
    {
        internal UInt16[,] ADCData;
        internal byte frequency;
        internal Polarization polarization;
//        internal UInt32 time;

        internal ADCFrequency(byte[] message, int indexFrequency)
        {
            ADCData = new UInt16[32, 4];
            //frequency = (byte) (indexFrequency + 1);
            //polarization = Polarization.HORIZONTAL;
            _time = (UInt32) (message[6] << 24 | message[7] << 16 | message[8] << 8 | message[9]);
            frequency = (byte)(message[10 + indexFrequency] & 0x7F);
            polarization = ((message[10 + indexFrequency] & 0x80) == 0x80) ? Polarization.VERTICAL : Polarization.HORIZONTAL;
            _message = new byte[256];
            Array.Copy(message, 14 + indexFrequency * 256, _message, 0, 256);
            //Array.Copy(message, 6 + indexFrequency * 256, _message, 0, 256);
            for (int i = 0; i < 32; ++i)
            {
                for (int j = 0; j < 4; ++j)
                {
                    ADCData[i, j] = Response.GetValue(_message, i * 4 * 2 + j * 2, i * 4 * 2 + j * 2 + 1);
                }
            }            
        }

        internal override string Print(bool number, bool time, bool data)
        {
            string message = "";

            foreach (byte b in _message)
            {
                message += string.Format("0x{0:X2} ", b);
            }
            return message;
        }

        internal UInt16 GetADCData(int period, int channell)
        {
            return ADCData[period, channell];
        }
    }
}
