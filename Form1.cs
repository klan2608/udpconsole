﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using ZedGraph;
using UPDConsole.Protocols;
using System.Threading;

namespace UPDConsole
{
    public partial class Form1 : Form
    {
        internal static Object locker = new Object();
        internal static bool flagUpdateLable = false;
        internal delegate void UpdateResponse(Response response);
        internal delegate void Update();
        internal delegate void UpdateStr(string str);
        internal delegate void UpdateState(bool state);

        public static uint numberOfPackage = 1;
        public static int rawADCStartNumber = 0;
        public static int rawADCPreveiwNumber = 0;
        public static int rawADCCounterTotal = 0;
        public static int rawADCCounterError = 0;
        public static int rawGPSStartNumber = 0;
        public static int rawGPSPreveiwNumber = 0;
        public static int rawGPSCounterTotal = 0;
        public static int rawGPSCounterError = 0;

        internal int frequencyNumber = 1;
        internal Polarization frequencyPolarization = Polarization.HORIZONTAL;
        internal UpdateResponse UpdateADC;
        internal Update UpdateADCQueue;
        internal UpdateResponse UpdateGPS;
        internal UpdateStr UpdateString;
        internal UpdateState MyDelegateState;
        internal Network _connection;
        internal MessageConfigurator _messageConfigurator;
        internal int _numberOfCheckedMessage;

        internal List<Response> responseQueue;
        internal List<ADCFrequency> _graphicList;
        internal bool flagUpdateGraph;
        internal UInt32 timeStartGraph;
        internal Thread process;
        internal bool isRun;

        public Form1()
        {
            InitializeComponent();

            CreateADCLable();

            _graphicList = new List<ADCFrequency>();
            flagUpdateGraph = false;
            timeStartGraph = 0;

            UpdateADC = new UpdateResponse(SetResponseInQueue);
            UpdateADCQueue = new Update(UpdateConsoleADCQueue);
            UpdateGPS = new UpdateResponse(UpdateConsoleGPS);
            UpdateString = new UpdateStr(UpdateConsoleString);
            MyDelegateState = new UpdateState(UpdateConnect);
            _connection = new Network(this);
            _messageConfigurator = new MessageConfigurator(_connection);
            _numberOfCheckedMessage = 1;
            UpdateCounterADC(rawADCCounterTotal, rawADCCounterError);
            UpdateCounterGPS(rawGPSCounterTotal, rawGPSCounterError);
            progressBarGraph.Visible = false;

            responseQueue = new List<Response>();
            process = new Thread(processMethod);
            isRun = true;
            process.Start();
        }

        private void processMethod()
        {
            try
            {
                while (isRun)
                {
                    Thread.Sleep(100);
                    if (!isRun)
                    {
                        break;
                    }
                    this.Invoke(UpdateADCQueue);
                }
            }
            catch (Exception)
            {
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            GraphPane myPane = zg1.GraphPane;

            myPane.Title.Text = "";
            myPane.XAxis.Title.Text = "Время, мс";
            myPane.YAxis.Title.Text = "Значение АЦП";

            myPane.XAxis.MajorGrid.IsVisible = true;
            myPane.XAxis.MinorGrid.IsVisible = true;

            myPane.YAxis.MajorTic.IsOpposite = false;
            myPane.YAxis.MinorTic.IsOpposite = false;
            myPane.YAxis.MajorGrid.IsZeroLine = false;
            myPane.YAxis.Scale.Align = AlignP.Inside;
            myPane.YAxis.Scale.Min = 0;
            myPane.YAxis.Scale.Max = 4096;

            myPane.Chart.Fill = new Fill(Color.White, Color.LightGray, 45.0f);

            zg1.IsShowHScrollBar = true;
            zg1.IsShowVScrollBar = true;
            zg1.IsAutoScrollRange = true;

            zg1.IsShowPointValues = true;
            zg1.PointValueEvent += new ZedGraphControl.PointValueHandler(MyPointValueHandler);

            SetSize();

            zg1.AxisChange();
            zg1.Invalidate();
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            SetSize();
        }

        private void SetSize()
        {
            zg1.Location = new Point(10, 10);
            zg1.Size = new Size(this.ClientRectangle.Width - 20,
                    this.ClientRectangle.Height - 20);
        }

        private string MyPointValueHandler(ZedGraphControl control, GraphPane pane,
                CurveItem curve, int iPt)
        {
            PointPair pt = curve[iPt];
            return "Значение: " + pt.Y.ToString("f2") + " Время: " + pt.X.ToString("f1") + " мс";
        }

        internal void UpdateConsoleString(string str)
        {
            txbReceiveADCData.AppendText(str);
        }

        private void CreateADCLable()
        {
            for (int i = 0; i < 4; ++i)
            {
                GroupBox gbxChannell = new GroupBox();
                gbxChannell.Name = "gbxChannell" + (i + 1).ToString();
                gbxChannell.Text = Resources.ChannellGroupBoxName + (i + 1).ToString();
                gbxChannell.Dock = DockStyle.Fill;
                tblADCData.Controls.Add(gbxChannell, i, 0);

                TableLayoutPanel tblChannell = new TableLayoutPanel();
                tblChannell.Name = "tblChannell" + (i + 1).ToString();
                tblChannell.ColumnCount = 1;
                tblChannell.RowCount = 32;
                tblChannell.Dock = DockStyle.Fill;
                gbxChannell.Controls.Add(tblChannell);

                for (int j = 0; j < 32; ++j)
                {
                    System.Windows.Forms.Label lblCountdown = new System.Windows.Forms.Label();
                    lblCountdown.Name = "lblCountdown" + (j + 1).ToString() + "_" + (i + 1).ToString();
                    lblCountdown.Text = Resources.NotAvailable;
                    lblCountdown.Dock = DockStyle.Fill;
                    lblCountdown.TextAlign = ContentAlignment.MiddleCenter;
                    tblChannell.RowStyles.Add(new RowStyle(SizeType.Percent, 100));
                    tblChannell.Controls.Add(lblCountdown, 0, j);
                }
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            _connection.Disconnect();
            isRun = false;
            _connection = null;
        }

        public void UpdateConnect(bool state)
        {
            menuConnect.Enabled = !state;
            menuDisconnect.Enabled = state;
        }

        internal void UpdateConsoleGPS(Response response)
        {
            try
            {
                if (rawGPSStartNumber == 0)
                {
                    rawGPSStartNumber = response._messageNumber;
                }
                rawGPSCounterTotal = response._messageNumber - rawGPSStartNumber + 1;

                if (rawGPSPreveiwNumber != 0)
                {
                    if (response._messageNumber - rawGPSPreveiwNumber > 1)
                    {
                        rawGPSCounterError += response._messageNumber - rawGPSPreveiwNumber - 1;
                    }
                }
                rawGPSPreveiwNumber = response._messageNumber;


                txbReceiveGPSData.AppendText(response.Print(chbxRawGPSVeiwNumber.Checked, chbxRawGPSVeiwTime.Checked, chbxRawGPSVeiwData.Checked));
                if (txbReceiveGPSData.Text.Length > 40000)
                {
                    txbReceiveGPSData.Text = txbReceiveGPSData.Text.Remove(0, txbReceiveGPSData.Text.Length - 40000);
                }
                UpdateCounterGPS(rawGPSCounterTotal, rawGPSCounterError);

                GPS gps = (response as GPS);
                lblGPSTime.Text = gps.Time;
                lblGPSLatitude.Text = gps.Latitude;
                lblGPSLongtitude.Text = gps.Longtitude;
                lblGPSAltitude.Text = gps.Altitude;
            }
            catch(Exception)
            {
            }
        }

        internal void SetResponseInQueue(Response response)
        {
            try
            {
                lock (locker)
                {
                    responseQueue.Add(response);
                }
            }
            catch (Exception)
            {
            }
        }

        internal void UpdateConsoleADCQueue()
        {
            try
            {
                lock (locker)
                {
                    foreach (Response response in responseQueue)
                    {
                        UpdateConsoleADC(response);
                    }
                    responseQueue.RemoveAll(r => true);
                }
            }
            catch (Exception)
            {
            }
        }

        internal void UpdateConsoleADC(Response response)
        {
            try
            {
                if (rawADCStartNumber == 0)
                {
                    rawADCStartNumber = response._messageNumber;
                }
                rawADCCounterTotal = response._messageNumber - rawADCStartNumber + 1;

                if (rawADCPreveiwNumber != 0)
                {
                    if (response._messageNumber - rawADCPreveiwNumber > 1)
                    {
                        rawADCCounterError += response._messageNumber - rawADCPreveiwNumber - 1;
                    }
                }
                rawADCPreveiwNumber = response._messageNumber;

                txbReceiveADCData.AppendText(response.Print(chbxRawADCVeiwNumber.Checked, chbxRawADCVeiwTime.Checked, chbxRawADCVeiwData.Checked));
                if (txbReceiveADCData.Text.Length > 40000)
                {
                    txbReceiveADCData.Text = txbReceiveADCData.Text.Remove(0, txbReceiveADCData.Text.Length - 40000);
                }
                UpdateCounterADC(rawADCCounterTotal, rawADCCounterError);

                int j = 0;
                int k = 0;

                ADCFrequency currentFrequency;
                if (frequencyPolarization != Polarization.BOTH)
                {
                    currentFrequency = (response as ADC)._frequency.Find(f => f.frequency == frequencyNumber && f.polarization == frequencyPolarization);
                }
                else
                {
                    currentFrequency = (response as ADC)._frequency.Find(f => f.frequency == frequencyNumber);
                }

                if (flagUpdateLable)
                {
                    if (currentFrequency != null)
                    {
                        foreach (GroupBox gbx in tblADCData.Controls)
                        {
                            foreach (System.Windows.Forms.Label lbl in gbx.Controls[0].Controls)
                            {
                                lbl.Text = string.Format(Resources.CountdownName + "{0:D2}   0x{1:X4}", k + 1, currentFrequency.ADCData[k, j]);
                                ++k;
                            }
                            k = 0;
                            ++j;
                        }
                    }
                }

                if (flagUpdateGraph)
                {
                    if (currentFrequency != null)
                    {
                        if (timeStartGraph == 0)
                        {
                            timeStartGraph = currentFrequency._time;
                        }
                        _graphicList.Add(currentFrequency);

                        UpdateProgress((int)(currentFrequency._time - timeStartGraph), (int)(txbTimeADC.Value * 1000));

                        if (currentFrequency._time - timeStartGraph >= (txbTimeADC.Value * 1000))
                        {
                            StopGraph();
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        private void UpdateProgress(int currentValue, int maxValue)
        {
            progressBarGraph.Value = currentValue * 100 / maxValue;
        }

        private void StartGraph()
        {
            flagUpdateGraph = true;
            timeStartGraph = 0;
            btnStartStopDraw.Text = "Остановить";
            _graphicList.RemoveAll(f => true);
            progressBarGraph.Value = 0;
            progressBarGraph.Visible = true;
        }

        private void StopGraph()
        {
            flagUpdateGraph = false;
            btnStartStopDraw.Text = "Построить график";
            progressBarGraph.Visible = false;

            GraphPane myPane = zg1.GraphPane;

            PointPairList list = new PointPairList();
            myPane.CurveList.RemoveAll(c => true);

            if (_graphicList.Count > 0)
            {
                foreach (ADCFrequency frequency in _graphicList)
                {
                    list.Add(frequency._time, frequency.ADCData[(int)(txbPeriodADC.Value - 1), (int)(txbChannellADC.Value - 1)]);
                }

                LineItem myCurve = myPane.AddCurve("ADC",
                    list, Color.Red, SymbolType.Diamond);
                myCurve.Symbol.Fill = new Fill(Color.White);

                zg1.AxisChange();
                zg1.Invalidate();
            }
        }

        private void btnTransmiteData_Click(object sender, EventArgs e)
        {
            _connection.Send(System.Text.Encoding.ASCII.GetBytes(txbTransmiteData.Text));
        }

        private void menuConnect_Click(object sender, EventArgs e)
        {
            _connection.Connect();
        }

        private void menuDisconnect_Click(object sender, EventArgs e)
        {
            _connection.Disconnect();
        }

        private void rbtnMessageType_CheckedChanged(object sender, EventArgs e)
        {
            pnlMessageEmpty.Enabled = rbtnEmpty.Checked;
            pnlMessageControlGPS.Enabled = rbtnControlGPS.Checked;
            pnlMessageControlFSMC.Enabled = rbtnControlFSMC.Checked;
            pnlMessageConfigRegister.Enabled = rbtnConfigRegister.Checked;
        }

        private void btnApplyMessage_Click(object sender, EventArgs e)
        {
            try
            {
                MyMessage message;
                if (rbtnEmpty.Checked)
                {
                    _messageConfigurator.Remove(_numberOfCheckedMessage);
                    System.Windows.Forms.Label lable = tblMessageList.Controls.Find("lblMessage" + _numberOfCheckedMessage.ToString(), true).GetValue(0) as System.Windows.Forms.Label;
                    lable.Text = _messageConfigurator.Print(_numberOfCheckedMessage);
                    System.Windows.Forms.Label lableDelay = tblMessageList.Controls.Find("lblDelay" + _numberOfCheckedMessage.ToString(), true).GetValue(0) as System.Windows.Forms.Label;
                    lableDelay.Text = "Не передается";
                }
                else if (rbtnConfigRegister.Checked)
                {
                    message = new ConfigRegistrMessage(chbxBitTest.Checked, chbxBitTestMode.Checked, chbxBitTxOn.Checked, chbxBitEnACQ.Checked, (byte)numBitsFrq.Value, chbxBitManual.Checked, (int)numDelay.Value);
                    _messageConfigurator.Add(message, _numberOfCheckedMessage);
                    System.Windows.Forms.Label lable = tblMessageList.Controls.Find("lblMessage" + _numberOfCheckedMessage.ToString(), true).GetValue(0) as System.Windows.Forms.Label;
                    lable.Text = _messageConfigurator.Print(_numberOfCheckedMessage);
                    System.Windows.Forms.Label lableDelay = tblMessageList.Controls.Find("lblDelay" + _numberOfCheckedMessage.ToString(), true).GetValue(0) as System.Windows.Forms.Label;
                    lableDelay.Text = message._delay.ToString();
                }
                else if (rbtnControlFSMC.Checked)
                {
                    message = new ControlFSMCMessage(rbtnFSMCOn.Checked, (int)numDelay.Value);
                    _messageConfigurator.Add(message, _numberOfCheckedMessage);
                    System.Windows.Forms.Label lable = tblMessageList.Controls.Find("lblMessage" + _numberOfCheckedMessage.ToString(), true).GetValue(0) as System.Windows.Forms.Label;
                    lable.Text = _messageConfigurator.Print(_numberOfCheckedMessage);
                    System.Windows.Forms.Label lableDelay = tblMessageList.Controls.Find("lblDelay" + _numberOfCheckedMessage.ToString(), true).GetValue(0) as System.Windows.Forms.Label;
                    lableDelay.Text = message._delay.ToString();
                }
                else if (rbtnControlGPS.Checked)
                {
                    message = new ControlGPSMessage(rbtnGPSOn.Checked, (int)numDelay.Value);
                    _messageConfigurator.Add(message, _numberOfCheckedMessage);
                    System.Windows.Forms.Label lable = tblMessageList.Controls.Find("lblMessage" + _numberOfCheckedMessage.ToString(), true).GetValue(0) as System.Windows.Forms.Label;
                    lable.Text = _messageConfigurator.Print(_numberOfCheckedMessage);
                    System.Windows.Forms.Label lableDelay = tblMessageList.Controls.Find("lblDelay" + _numberOfCheckedMessage.ToString(), true).GetValue(0) as System.Windows.Forms.Label;
                    lableDelay.Text = message._delay.ToString();
                }
            }
            catch (Exception)
            {
            }
        }

        private void rbtnMessage_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton button = sender as RadioButton;
            if(button.Checked)
            {
                _numberOfCheckedMessage = int.Parse(button.Text.Replace("№", ""));
                MyMessage message = _messageConfigurator.Get(_numberOfCheckedMessage);
                UpdateMessageLable(message);
            }
        }

        private void UpdateMessageLable(TypeMassage type, bool stateFSMC, bool stateGPS, bool test, bool testMode, bool enACQ, bool txOn, byte frq, bool man, int delay)
        {
            rbtnFSMCOn.Checked = stateFSMC;
            rbtnFSMCOff.Checked = !stateFSMC;
            rbtnGPSOn.Checked = stateGPS;
            rbtnGPSOff.Checked = !stateGPS;

            chbxBitTest.Checked = test;
            chbxBitTestMode.Checked = testMode;
            chbxBitEnACQ.Checked = enACQ;
            chbxBitTxOn.Checked = txOn;
            chbxBitManual.Checked = man;
            numBitsFrq.Value = frq;
            numDelay.Value = delay;

            switch (type)
            {
                case TypeMassage.EMPTY:
                    rbtnEmpty.Checked = true;
                    break;
                case TypeMassage.CONFIG_REGISTER:
                    rbtnConfigRegister.Checked = true;
                    break;
                case TypeMassage.CONTROL_FSMC:
                    rbtnControlFSMC.Checked = true;
                    break;
                case TypeMassage.CONTROL_GPS:
                    rbtnControlGPS.Checked = true;
                    break;
                default:
                    break;
            }
        }

        private void UpdateMessageLable(MyMessage message)
        {
            if (message is ConfigRegistrMessage)
            {
                ConfigRegistrMessage mes = message as ConfigRegistrMessage;
                UpdateMessageLable(TypeMassage.CONFIG_REGISTER, true, true, mes._test, mes._testMode, mes._enACQ, mes._txOn, mes._frq, mes._man, mes._delay);
            }
            else if(message is ControlFSMCMessage)
            {
                ControlFSMCMessage mes = message as ControlFSMCMessage;
                UpdateMessageLable(TypeMassage.CONTROL_FSMC, mes._state, true, false, false, false, false, 0, false, mes._delay);
            }
            else if (message is ControlGPSMessage)
            {
                ControlGPSMessage mes = message as ControlGPSMessage;
                UpdateMessageLable(TypeMassage.CONTROL_GPS, mes._state, true, false, false, false, false, 0, false, mes._delay);
            }
            else
            {
                UpdateMessageLable(TypeMassage.EMPTY, true, true, false, false, false, false, 0, false, 0);
            }
        }

        private void btnTaskStart_Click(object sender, EventArgs e)
        {
            grbxMessage.Enabled = false;
            _messageConfigurator.StartSending(chbxCircylarSending.Checked);
        }

        private void btnTaskStop_Click(object sender, EventArgs e)
        {
            grbxMessage.Enabled = true;
            _messageConfigurator.Stop();
        }

        private void btnSendMessage_Click(object sender, EventArgs e)
        {
            Button button = sender as Button;
            int number = int.Parse(button.Text.Replace("Отправить №", ""));
            _messageConfigurator.Send(_numberOfCheckedMessage);
        }

        private void btnRawADCCleare_Click(object sender, EventArgs e)
        {
            txbReceiveADCData.Text = "";
        }

        private void btnRawGPSClearn_Click(object sender, EventArgs e)
        {
            txbReceiveGPSData.Text = "";
        }

        private void btnRawADCCounterReset_Click(object sender, EventArgs e)
        {
            rawADCCounterTotal = 0;
            rawADCCounterError = 0;
            rawADCPreveiwNumber = 0;
            rawADCStartNumber = 0;
            UpdateCounterADC(rawADCCounterTotal, rawADCCounterError);
        }

        private void UpdateCounterADC(int total, int error)
        {
            lblRawADCCounterTotal.Text = string.Format("Количество сообщений\n{0}", total);
            lblRawADCCounterError.Text = string.Format("Из них потеряно\n{0}", error);
        }

        private void UpdateCounterGPS(int total, int error)
        {
            lblRawGPSCounterTotal.Text = string.Format("Количество сообщений\n{0}", total);
            lblRawGPSCounterError.Text = string.Format("Из них потеряно\n{0}", error);
        }

        private void btnRawGPSCounterReset_Click(object sender, EventArgs e)
        {
            rawGPSCounterTotal = 0;
            rawGPSCounterError = 0;
            rawGPSPreveiwNumber = 0;
            rawGPSStartNumber = 0;
            UpdateCounterGPS(rawGPSCounterTotal, rawGPSCounterError);
        }

        private void btnPeriodADC_MouseDown(object sender, MouseEventArgs e)
        {
            if (rbtnFrequencyHorizontal.Checked)
            {
                frequencyPolarization = Polarization.HORIZONTAL;
            }
            else
            {
                frequencyPolarization = Polarization.VERTICAL;
            }

            frequencyNumber = (int)txbFrequencyADC.Value;

            flagUpdateLable = true;
        }

        private void btnPeriodADC_MouseUp(object sender, MouseEventArgs e)
        {
            flagUpdateLable = false;
        }

        private void btnStartStopDraw_Click(object sender, EventArgs e)
        {
            if (flagUpdateGraph)
            {
                StopGraph();
            }
            else
            {
                StartGraph();
            }
        }
    }
}
