﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;
using UPDConsole.Protocols;

namespace UPDConsole
{
    internal class Network
    {
        bool _connect;
        Form1 _mainWindow;
        Thread _receiveProcess;
        bool _processIsWorking = true;
        UdpClient _socket;
        SettingsConfigData _settings;
        IPAddress _address;
        IPEndPoint _endPoint;

        public Network(Form1 mainWindow)
        {
            _connect = false;
            _mainWindow = mainWindow;
        }

        public void receiveHandler()
        {
            Console.WriteLine("Start....");
            while (_processIsWorking)
            {
                try
                {
                    IPEndPoint _endPointDevice = _endPoint;
                    Byte[] receiveBytes = _socket.Receive(ref _endPointDevice);
                    if (receiveBytes != null)
                    {
                        Response response = Response.GetResponse(receiveBytes);
                        if (response is ADC)
                        {
                             _mainWindow.Invoke(_mainWindow.UpdateADC, response);
                        }
                        else if (response is GPS)
                        {
                            _mainWindow.Invoke(_mainWindow.UpdateGPS, response);
                        }
                    }
                }
                catch (Exception exc)
                {
                    Console.WriteLine(exc.ToString());
                }
            }
            Console.WriteLine("Stop....");
        }

        public void Send(byte[] message)
        {
            try
            {
                if (!_connect)
                {
                    Connect();
                }
                _socket.Send(message, message.Length, _endPoint);
            }
            catch (Exception)
            {
            }
        }

        public void Disconnect()
        {
            try
            {
                if (_connect)
                {
                    _processIsWorking = false;
                    _connect = false;
                    _mainWindow.Invoke(_mainWindow.MyDelegateState, false);
                    try
                    {
                        _socket.Close();
                    }
                    catch (Exception exc)
                    {
                        Console.WriteLine(exc.ToString());
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        public void Connect()
        {
            try
            {
                if (!_connect)
                {
                    _receiveProcess = new Thread(receiveHandler);
                    _settings = new SettingsConfigData(true);
                    _socket = new UdpClient(_settings.PCPort);
                    _address = IPAddress.Parse(_settings.ControllerIPAddress);
                    _endPoint = new IPEndPoint(_address, _settings.ControllerPort);
                    _processIsWorking = true;
                    _connect = true;
                    _mainWindow.Invoke(_mainWindow.MyDelegateState, true);
                    _receiveProcess.Start();
                }
            }
            catch (Exception)
            {
            }
        }
    }
}
