﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;
using System.Xml.Serialization;
using System.IO;

namespace UPDConsole
{
    public partial class Settings : Form
    {
        internal SerialPort _serial;
        internal Form1 _mainWindow;
        private SettingsConfigData _settings;
        public Settings(Form1 mainWindow)
        {
            InitializeComponent();

            _mainWindow = mainWindow;

            lblComPortName.Text = Resources.ComPortName;
            lblComBoudRateName.Text = Resources.ComBoudRateName;
            lblControllerIPAddressName.Text = Resources.ControllerIPAddressName;
            lblControllerIPMaskName.Text = Resources.ControllerIPMaskName;
            lblControllerReceivePortName.Text = Resources.ControllerReceivePortName;
            lblControllerTransmitePortName.Text = Resources.ControllerTransmitePortName;
            lblPCIPAddressName.Text = Resources.PCIPAddressName;
            lblPCReceivePortName.Text = Resources.PCReceivePortName;
            lblPCTransmitePortName.Text = Resources.PCTransmitePortName;

            btnConnect.Text = Resources.ConnectButtonName;
            btnReadConfiguration.Text = Resources.ReadConfigurationButtonName;
            btnWriteConfiguration.Text = Resources.WriteCinfigurationButtonName;
            btnReadConfiguration.Enabled = false;
            btnWriteConfiguration.Enabled = false;

            ChangeState(Resources.StateDisconnectName);

            _settings = new SettingsConfigData(true);
            writeConfigToUI();
        }

        private void readConfigFromUI()
        {
            _settings.SetConfigData(cbxComPort.Text, cbxBoudRate.Text,
                txbControllerIPAddress.Text, txbControllerIPMask.Text,
                txbControllerTransmitePort.Text, txbPCIPAddress.Text, txbPCReceivePort.Text);
        }

        private void writeConfigToUI()
        {
            cbxComPort.Text = _settings.ComPort;
            cbxBoudRate.Text = _settings.BaudRate.ToString();
            txbControllerIPAddress.Text = _settings.ControllerIPAddress;
            txbControllerIPMask.Text = _settings.ControllerIPMask;
            txbControllerTransmitePort.Text = _settings.ControllerPort.ToString();
            txbPCIPAddress.Text = _settings.PCIPAddress;
            txbPCReceivePort.Text = _settings.PCPort.ToString();
        }

        public void ChangeState(string newState)
        {
            if (newState == Resources.StateDisconnectName || newState == Resources.StateErrorConnectionName)
            {
                lblState.ForeColor = System.Drawing.Color.Red;
            }
            else if (newState == Resources.StateEditConfigurationName)
            {
                lblState.ForeColor = System.Drawing.Color.Yellow;
            }
            else
            {
                lblState.ForeColor = System.Drawing.Color.Green;
            }
            lblState.Text = newState;
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (btnConnect.Text == Resources.ConnectButtonName)
            {
                try
                {
                    int baudRate = Convert.ToInt32(cbxBoudRate.Text);
                    _serial = new SerialPort(cbxComPort.Text, baudRate);
                    if (!_serial.IsOpen)
                    {
                        _serial.Open();
                    }
                    btnConnect.Text = Resources.DisconnectButtonName;
                    ChangeState(Resources.StateConnectName);
                    btnReadConfiguration.Enabled = true;
                    btnWriteConfiguration.Enabled = true;
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }
            }
            else
            {
                try
                {
                    if (_serial.IsOpen)
                    {
                        _serial.Close();
                    }
                    btnConnect.Text = Resources.ConnectButtonName;
                    ChangeState(Resources.StateDisconnectName);
                    btnReadConfiguration.Enabled = false;
                    btnWriteConfiguration.Enabled = false;
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }
            }
        }

        private void btnReadConfiguration_Click(object sender, EventArgs e)
        {
            ChangeState(Resources.StateReadConfigurationName);
        }

        private void btnWriteConfiguration_Click(object sender, EventArgs e)
        {
            ChangeState(Resources.StateWriteConfigurationName);
            try
            {
                byte[] message = Protocol.SettingsMessage(_settings);
                Console.WriteLine(message);
                _serial.Write(message, 0, message.Length);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void Settings_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_serial != null && _serial.IsOpen)
            {
                _serial.Close();
            }
            readConfigFromUI();
            _settings.writeToFile();
            _mainWindow._connection.Connect();
        }
    }
}
