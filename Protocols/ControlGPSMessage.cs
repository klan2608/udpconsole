﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UPDConsole.Protocols
{
    internal class ControlGPSMessage : MyMessage
    {
        internal bool _state;
        internal ControlGPSMessage(bool state, int delay)
        {
            _state = state;
            _delay = delay;
            _message = new byte[8];
            _message[0] = (byte)(Consts.PACKAGE_HEAD_SETTINGS >> 8);
            _message[1] = (byte)(Consts.PACKAGE_HEAD_SETTINGS & 0xFF);
            _message[2] = 0;
            _message[3] = 0;
            _message[4] = (byte)(Consts.PACKAGE_SETTINGS_TYPE_CONTROL_GPS >> 8);
            _message[5] = (byte)(Consts.PACKAGE_SETTINGS_TYPE_CONTROL_GPS & 0xFF);
            _message[6] = 0;
            _message[7] = (byte)(_state ? 1 : 0);
        }
    }
}
