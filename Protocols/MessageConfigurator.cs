﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace UPDConsole.Protocols
{
    internal class MessageConfigurator
    {
        internal MyMessage[] _listMessage;
        internal Network _sender;
        internal Thread _proccess;
        internal bool _cyrcular = false;
        internal bool _running = false;

        internal MessageConfigurator(Network sender)
        {
            _sender = sender;
            _listMessage = new MyMessage[10];
        }

        internal void Add(MyMessage message, int index)
        {
            if (index > 0 && index <= 10 && message != null)
            {
                _listMessage[index - 1] = message;
            }
        }

        internal MyMessage Get(int index)
        {
            MyMessage message = null;
            if (index > 0 && index <= 10)
            {
                message = _listMessage[index - 1];
            }
            return message;
        }

        internal void Send(int index)
        {
            MyMessage message = null;
            if (index > 0 && index <= 10)
            {
                message = _listMessage[index - 1];
            }
            if (message != null)
            {
                _sender.Send(message._message);
            }
        }

        internal void Remove(int index)
        {
            if (index > 0 && index <= 10)
            {
                _listMessage[index - 1] = null;
            }
        }

        internal string Print(int index)
        {
            string message = "";
            if (index > 0 && index <= 10)
            {
                if (_listMessage[index - 1] == null)
                {
                    message = "Нет сообщения";
                }
                else
                {
                    foreach(byte b in _listMessage[index-1]._message)
                    {
                        message += string.Format("{0:X2} ", b);
                    }
                }
            }
            return message;
        }

        internal void StartSending(bool cyrcular)
        {
            Stop();
            _proccess = new Thread(Run);
            _running = true;
            _cyrcular = cyrcular;
            _proccess.Start();
        }

        internal void Stop()
        {
            _running = false;
        }

        internal void Run()
        {
            while (_running)
            {
                foreach(MyMessage mes in _listMessage)
                {
                    if (mes != null)
                    {
                        _sender.Send(mes._message);
                        if (mes._delay > 0)
                        {
                            Thread.Sleep(mes._delay);
                        }
                    }
                    if (!_running)
                    {
                        break;
                    }
                }
                if (!_cyrcular)
                {
                    break;
                }
            }
        }
    }
}
