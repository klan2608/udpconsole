﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace UPDConsole
{
    public static class Protocol
    {
        public static byte[] SettingsMessage(SettingsConfigData setting)
        {
            byte[] message = new byte[22];
            message[0] = 0x33;
            message[1] = 0x05;
            IPAddress controllerIP = IPAddress.Parse(setting.ControllerIPAddress);
            Array.Copy(controllerIP.GetAddressBytes(), 0, message, 2, 4);
            IPAddress controllerMask = IPAddress.Parse(setting.ControllerIPMask);
            Array.Copy(controllerMask.GetAddressBytes(), 0, message, 6, 4);
//            message[10] = (byte)(setting.ControllerReceivePort >> 8);
//            message[11] = (byte)(setting.ControllerReceivePort);
            message[12] = (byte)(setting.ControllerPort >> 8);
            message[13] = (byte)(setting.ControllerPort);
            IPAddress computerIP = IPAddress.Parse(setting.PCIPAddress);
            Array.Copy(computerIP.GetAddressBytes(), 0, message, 14, 4);
            message[18] = (byte)(setting.PCPort >> 8);
            message[19] = (byte)(setting.PCPort);
//            message[20] = (byte)(setting.PCTransmitePort >> 8);
//            message[21] = (byte)(setting.PCTransmitePort);
            return message;
        }
    }
}
