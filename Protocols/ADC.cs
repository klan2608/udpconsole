﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UPDConsole.Protocols
{
    internal class ADC : Response
    {
        internal List<ADCFrequency> _frequency;

        internal ADC(byte[] message)
        {
            _frequency = new List<ADCFrequency>();
            _frequency.Add(new ADCFrequency(message, 0));
            _frequency.Add(new ADCFrequency(message, 1));
            _frequency.Add(new ADCFrequency(message, 2));
            _frequency.Add(new ADCFrequency(message, 3));

            _messageNumber = (int)GetValue(message, 2, 3, 4, 5);
            _time = GetValue(message, 6, 7, 8, 9);
            //_messageNumber = GetValue(message, 2, 3);
            //_time = GetValue(message, 4, 5);
        }

        internal override string Print(bool number, bool time, bool data)
        {
            
            string message = "\n";

            if (number)
            {
                message += string.Format("<< Пакет № {0} ", _messageNumber);
            }

            if (time)
            {
                message += string.Format("Время {0} ", _time);
            }

            if (data)
            {
                string messageStr = "";
                foreach (ADCFrequency frequency in _frequency)
                {
                    messageStr += string.Format("\nПоляризация {0} Частота {1}\n{2}",
                        frequency.polarization, frequency.frequency, frequency.Print(false, false, false));
                }
                message += messageStr;
            }

            return message;
        }
    }
}
