﻿namespace UPDConsole
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.gbxRawADCName = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.txbReceiveADCData = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.chbxRawADCVeiwNumber = new System.Windows.Forms.CheckBox();
            this.chbxRawADCVeiwTime = new System.Windows.Forms.CheckBox();
            this.chbxRawADCVeiwData = new System.Windows.Forms.CheckBox();
            this.btnRawADCCleare = new System.Windows.Forms.Button();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.lblRawADCCounterTotal = new System.Windows.Forms.Label();
            this.lblRawADCCounterError = new System.Windows.Forms.Label();
            this.btnRawADCCounterReset = new System.Windows.Forms.Button();
            this.gbxRawGPSName = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel18 = new System.Windows.Forms.TableLayoutPanel();
            this.lblRawGPSCounterTotal = new System.Windows.Forms.Label();
            this.lblRawGPSCounterError = new System.Windows.Forms.Label();
            this.btnRawGPSCounterReset = new System.Windows.Forms.Button();
            this.txbReceiveGPSData = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanel17 = new System.Windows.Forms.TableLayoutPanel();
            this.chbxRawGPSVeiwNumber = new System.Windows.Forms.CheckBox();
            this.chbxRawGPSVeiwTime = new System.Windows.Forms.CheckBox();
            this.chbxRawGPSVeiwData = new System.Windows.Forms.CheckBox();
            this.btnRawGPSClearn = new System.Windows.Forms.Button();
            this.gbxTransmiteName = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.txbTransmiteData = new System.Windows.Forms.RichTextBox();
            this.btnTransmiteData = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuConnect = new System.Windows.Forms.ToolStripMenuItem();
            this.menuDisconnect = new System.Windows.Forms.ToolStripMenuItem();
            this.tabData = new System.Windows.Forms.TabControl();
            this.tabRAWDataPage = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel21 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.grbxMessage = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.rbtnEmpty = new System.Windows.Forms.RadioButton();
            this.rbtnConfigRegister = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.btnApplyMessage = new System.Windows.Forms.Button();
            this.numDelay = new System.Windows.Forms.NumericUpDown();
            this.lblDelay = new System.Windows.Forms.Label();
            this.rbtnControlFSMC = new System.Windows.Forms.RadioButton();
            this.rbtnControlGPS = new System.Windows.Forms.RadioButton();
            this.pnlMessageEmpty = new System.Windows.Forms.Panel();
            this.pnlMessageConfigRegister = new System.Windows.Forms.Panel();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.chbxBitTest = new System.Windows.Forms.CheckBox();
            this.chbxBitTestMode = new System.Windows.Forms.CheckBox();
            this.chbxBitTxOn = new System.Windows.Forms.CheckBox();
            this.chbxBitEnACQ = new System.Windows.Forms.CheckBox();
            this.lblFrequncyName = new System.Windows.Forms.Label();
            this.numBitsFrq = new System.Windows.Forms.NumericUpDown();
            this.chbxBitManual = new System.Windows.Forms.CheckBox();
            this.pnlMessageControlFSMC = new System.Windows.Forms.Panel();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.rbtnFSMCOn = new System.Windows.Forms.RadioButton();
            this.rbtnFSMCOff = new System.Windows.Forms.RadioButton();
            this.pnlMessageControlGPS = new System.Windows.Forms.Panel();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.rbtnGPSOn = new System.Windows.Forms.RadioButton();
            this.rbtnGPSOff = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.btnTaskClear = new System.Windows.Forms.Button();
            this.btnTaskStart = new System.Windows.Forms.Button();
            this.btnTaskStop = new System.Windows.Forms.Button();
            this.chbxCircylarSending = new System.Windows.Forms.CheckBox();
            this.grbxMessageList = new System.Windows.Forms.GroupBox();
            this.tblMessageList = new System.Windows.Forms.TableLayoutPanel();
            this.lblDelay10 = new System.Windows.Forms.Label();
            this.lblDelay9 = new System.Windows.Forms.Label();
            this.lblDelay8 = new System.Windows.Forms.Label();
            this.lblDelay7 = new System.Windows.Forms.Label();
            this.lblDelay6 = new System.Windows.Forms.Label();
            this.lblDelay5 = new System.Windows.Forms.Label();
            this.lblDelay4 = new System.Windows.Forms.Label();
            this.lblDelay3 = new System.Windows.Forms.Label();
            this.lblDelay2 = new System.Windows.Forms.Label();
            this.lblDelay1 = new System.Windows.Forms.Label();
            this.btnSendMessage1 = new System.Windows.Forms.Button();
            this.btnSendMessage2 = new System.Windows.Forms.Button();
            this.btnSendMessage3 = new System.Windows.Forms.Button();
            this.btnSendMessage4 = new System.Windows.Forms.Button();
            this.btnSendMessage5 = new System.Windows.Forms.Button();
            this.btnSendMessage6 = new System.Windows.Forms.Button();
            this.btnSendMessage7 = new System.Windows.Forms.Button();
            this.btnSendMessage8 = new System.Windows.Forms.Button();
            this.btnSendMessage9 = new System.Windows.Forms.Button();
            this.btnSendMessage10 = new System.Windows.Forms.Button();
            this.lblMessage1 = new System.Windows.Forms.Label();
            this.lblMessage2 = new System.Windows.Forms.Label();
            this.lblMessage3 = new System.Windows.Forms.Label();
            this.lblMessage4 = new System.Windows.Forms.Label();
            this.lblMessage5 = new System.Windows.Forms.Label();
            this.lblMessage6 = new System.Windows.Forms.Label();
            this.lblMessage7 = new System.Windows.Forms.Label();
            this.lblMessage8 = new System.Windows.Forms.Label();
            this.lblMessage9 = new System.Windows.Forms.Label();
            this.lblMessage10 = new System.Windows.Forms.Label();
            this.rbtnMessage1 = new System.Windows.Forms.RadioButton();
            this.rbtnMessage2 = new System.Windows.Forms.RadioButton();
            this.rbtnMessage3 = new System.Windows.Forms.RadioButton();
            this.rbtnMessage4 = new System.Windows.Forms.RadioButton();
            this.rbtnMessage5 = new System.Windows.Forms.RadioButton();
            this.rbtnMessage6 = new System.Windows.Forms.RadioButton();
            this.rbtnMessage7 = new System.Windows.Forms.RadioButton();
            this.rbtnMessage8 = new System.Windows.Forms.RadioButton();
            this.rbtnMessage9 = new System.Windows.Forms.RadioButton();
            this.rbtnMessage10 = new System.Windows.Forms.RadioButton();
            this.tabCalDataPage = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel22 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.gbxADCName = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.lblFrequencyADCName = new System.Windows.Forms.Label();
            this.txbFrequencyADC = new System.Windows.Forms.NumericUpDown();
            this.rbtnFrequencyHorizontal = new System.Windows.Forms.RadioButton();
            this.grbADCData = new System.Windows.Forms.GroupBox();
            this.tblADCData = new System.Windows.Forms.TableLayoutPanel();
            this.lblPeriodADCName = new System.Windows.Forms.Label();
            this.txbPeriodADC = new System.Windows.Forms.NumericUpDown();
            this.btnStartStopDraw = new System.Windows.Forms.Button();
            this.txbTimeADC = new System.Windows.Forms.NumericUpDown();
            this.lblTimeADCName = new System.Windows.Forms.Label();
            this.lblChannellADCName = new System.Windows.Forms.Label();
            this.btnFrequencyADCAllChannel = new System.Windows.Forms.Button();
            this.rbtnFrequencyVertical = new System.Windows.Forms.RadioButton();
            this.txbChannellADC = new System.Windows.Forms.NumericUpDown();
            this.gbxGPSName = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.lblGPSAltitude = new System.Windows.Forms.Label();
            this.lblGPSAltitudeName = new System.Windows.Forms.Label();
            this.lblGPSLongtitudeName = new System.Windows.Forms.Label();
            this.lblGPSLongtitude = new System.Windows.Forms.Label();
            this.lblGPSLatitudeName = new System.Windows.Forms.Label();
            this.lblGPSTimeName = new System.Windows.Forms.Label();
            this.lblGPSLatitude = new System.Windows.Forms.Label();
            this.lblGPSTime = new System.Windows.Forms.Label();
            this.zg1 = new ZedGraph.ZedGraphControl();
            this.progressBarGraph = new System.Windows.Forms.ProgressBar();
            this.tableLayoutPanel1.SuspendLayout();
            this.gbxRawADCName.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            this.gbxRawGPSName.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            this.tableLayoutPanel18.SuspendLayout();
            this.tableLayoutPanel17.SuspendLayout();
            this.gbxTransmiteName.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.tabData.SuspendLayout();
            this.tabRAWDataPage.SuspendLayout();
            this.tableLayoutPanel21.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.grbxMessage.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numDelay)).BeginInit();
            this.pnlMessageConfigRegister.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numBitsFrq)).BeginInit();
            this.pnlMessageControlFSMC.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.pnlMessageControlGPS.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.grbxMessageList.SuspendLayout();
            this.tblMessageList.SuspendLayout();
            this.tabCalDataPage.SuspendLayout();
            this.tableLayoutPanel22.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.gbxADCName.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txbFrequencyADC)).BeginInit();
            this.grbADCData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txbPeriodADC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbTimeADC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbChannellADC)).BeginInit();
            this.gbxGPSName.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 53.1677F));
            this.tableLayoutPanel1.Controls.Add(this.gbxRawADCName, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.gbxRawGPSName, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.gbxTransmiteName, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(539, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 66.86508F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.13492F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 57F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(633, 701);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // gbxRawADCName
            // 
            this.gbxRawADCName.Controls.Add(this.tableLayoutPanel13);
            this.gbxRawADCName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbxRawADCName.Location = new System.Drawing.Point(3, 3);
            this.gbxRawADCName.Name = "gbxRawADCName";
            this.gbxRawADCName.Size = new System.Drawing.Size(627, 424);
            this.gbxRawADCName.TabIndex = 5;
            this.gbxRawADCName.TabStop = false;
            this.gbxRawADCName.Text = "Данные АЦП";
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 1;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.Controls.Add(this.txbReceiveADCData, 0, 1);
            this.tableLayoutPanel13.Controls.Add(this.tableLayoutPanel14, 0, 0);
            this.tableLayoutPanel13.Controls.Add(this.tableLayoutPanel15, 0, 2);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 3;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(621, 405);
            this.tableLayoutPanel13.TabIndex = 0;
            // 
            // txbReceiveADCData
            // 
            this.txbReceiveADCData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txbReceiveADCData.Location = new System.Drawing.Point(3, 38);
            this.txbReceiveADCData.Name = "txbReceiveADCData";
            this.txbReceiveADCData.Size = new System.Drawing.Size(615, 328);
            this.txbReceiveADCData.TabIndex = 2;
            this.txbReceiveADCData.Text = "";
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 4;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 145F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 82F));
            this.tableLayoutPanel14.Controls.Add(this.chbxRawADCVeiwNumber, 0, 0);
            this.tableLayoutPanel14.Controls.Add(this.chbxRawADCVeiwTime, 1, 0);
            this.tableLayoutPanel14.Controls.Add(this.chbxRawADCVeiwData, 2, 0);
            this.tableLayoutPanel14.Controls.Add(this.btnRawADCCleare, 3, 0);
            this.tableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel14.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 1;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(615, 29);
            this.tableLayoutPanel14.TabIndex = 3;
            // 
            // chbxRawADCVeiwNumber
            // 
            this.chbxRawADCVeiwNumber.AutoSize = true;
            this.chbxRawADCVeiwNumber.Checked = true;
            this.chbxRawADCVeiwNumber.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbxRawADCVeiwNumber.Dock = System.Windows.Forms.DockStyle.Left;
            this.chbxRawADCVeiwNumber.Location = new System.Drawing.Point(3, 3);
            this.chbxRawADCVeiwNumber.Name = "chbxRawADCVeiwNumber";
            this.chbxRawADCVeiwNumber.Size = new System.Drawing.Size(98, 23);
            this.chbxRawADCVeiwNumber.TabIndex = 0;
            this.chbxRawADCVeiwNumber.Text = "Номер пакета";
            this.chbxRawADCVeiwNumber.UseVisualStyleBackColor = true;
            // 
            // chbxRawADCVeiwTime
            // 
            this.chbxRawADCVeiwTime.AutoSize = true;
            this.chbxRawADCVeiwTime.Checked = true;
            this.chbxRawADCVeiwTime.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbxRawADCVeiwTime.Dock = System.Windows.Forms.DockStyle.Left;
            this.chbxRawADCVeiwTime.Location = new System.Drawing.Point(123, 3);
            this.chbxRawADCVeiwTime.Name = "chbxRawADCVeiwTime";
            this.chbxRawADCVeiwTime.Size = new System.Drawing.Size(127, 23);
            this.chbxRawADCVeiwTime.TabIndex = 1;
            this.chbxRawADCVeiwTime.Text = "Время отправления";
            this.chbxRawADCVeiwTime.UseVisualStyleBackColor = true;
            // 
            // chbxRawADCVeiwData
            // 
            this.chbxRawADCVeiwData.AutoSize = true;
            this.chbxRawADCVeiwData.Checked = true;
            this.chbxRawADCVeiwData.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbxRawADCVeiwData.Dock = System.Windows.Forms.DockStyle.Left;
            this.chbxRawADCVeiwData.Location = new System.Drawing.Point(268, 3);
            this.chbxRawADCVeiwData.Name = "chbxRawADCVeiwData";
            this.chbxRawADCVeiwData.Size = new System.Drawing.Size(67, 23);
            this.chbxRawADCVeiwData.TabIndex = 1;
            this.chbxRawADCVeiwData.Text = "Данные";
            this.chbxRawADCVeiwData.UseVisualStyleBackColor = true;
            // 
            // btnRawADCCleare
            // 
            this.btnRawADCCleare.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnRawADCCleare.Location = new System.Drawing.Point(537, 3);
            this.btnRawADCCleare.Name = "btnRawADCCleare";
            this.btnRawADCCleare.Size = new System.Drawing.Size(75, 23);
            this.btnRawADCCleare.TabIndex = 2;
            this.btnRawADCCleare.Text = "Очистить";
            this.btnRawADCCleare.UseVisualStyleBackColor = true;
            this.btnRawADCCleare.Click += new System.EventHandler(this.btnRawADCCleare_Click);
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 3;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 53.87597F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 46.12403F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 93F));
            this.tableLayoutPanel15.Controls.Add(this.lblRawADCCounterTotal, 0, 0);
            this.tableLayoutPanel15.Controls.Add(this.lblRawADCCounterError, 1, 0);
            this.tableLayoutPanel15.Controls.Add(this.btnRawADCCounterReset, 2, 0);
            this.tableLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLayoutPanel15.Location = new System.Drawing.Point(3, 372);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 1;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(353, 30);
            this.tableLayoutPanel15.TabIndex = 4;
            // 
            // lblRawADCCounterTotal
            // 
            this.lblRawADCCounterTotal.AutoSize = true;
            this.lblRawADCCounterTotal.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblRawADCCounterTotal.Location = new System.Drawing.Point(3, 0);
            this.lblRawADCCounterTotal.Name = "lblRawADCCounterTotal";
            this.lblRawADCCounterTotal.Size = new System.Drawing.Size(126, 30);
            this.lblRawADCCounterTotal.TabIndex = 0;
            this.lblRawADCCounterTotal.Text = "Количество сообщений";
            // 
            // lblRawADCCounterError
            // 
            this.lblRawADCCounterError.AutoSize = true;
            this.lblRawADCCounterError.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblRawADCCounterError.Location = new System.Drawing.Point(143, 0);
            this.lblRawADCCounterError.Name = "lblRawADCCounterError";
            this.lblRawADCCounterError.Size = new System.Drawing.Size(91, 30);
            this.lblRawADCCounterError.TabIndex = 1;
            this.lblRawADCCounterError.Text = "Из них потеряно";
            // 
            // btnRawADCCounterReset
            // 
            this.btnRawADCCounterReset.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnRawADCCounterReset.Location = new System.Drawing.Point(262, 3);
            this.btnRawADCCounterReset.Name = "btnRawADCCounterReset";
            this.btnRawADCCounterReset.Size = new System.Drawing.Size(75, 24);
            this.btnRawADCCounterReset.TabIndex = 2;
            this.btnRawADCCounterReset.Text = "Сброс";
            this.btnRawADCCounterReset.UseVisualStyleBackColor = true;
            this.btnRawADCCounterReset.Click += new System.EventHandler(this.btnRawADCCounterReset_Click);
            // 
            // gbxRawGPSName
            // 
            this.gbxRawGPSName.Controls.Add(this.tableLayoutPanel16);
            this.gbxRawGPSName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbxRawGPSName.Location = new System.Drawing.Point(3, 433);
            this.gbxRawGPSName.Name = "gbxRawGPSName";
            this.gbxRawGPSName.Size = new System.Drawing.Size(627, 207);
            this.gbxRawGPSName.TabIndex = 6;
            this.gbxRawGPSName.TabStop = false;
            this.gbxRawGPSName.Text = "Данные GPS";
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.ColumnCount = 1;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel16.Controls.Add(this.tableLayoutPanel18, 0, 2);
            this.tableLayoutPanel16.Controls.Add(this.txbReceiveGPSData, 0, 1);
            this.tableLayoutPanel16.Controls.Add(this.tableLayoutPanel17, 0, 0);
            this.tableLayoutPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel16.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 3;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(621, 188);
            this.tableLayoutPanel16.TabIndex = 0;
            // 
            // tableLayoutPanel18
            // 
            this.tableLayoutPanel18.ColumnCount = 3;
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 53.87597F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 46.12403F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 93F));
            this.tableLayoutPanel18.Controls.Add(this.lblRawGPSCounterTotal, 0, 0);
            this.tableLayoutPanel18.Controls.Add(this.lblRawGPSCounterError, 1, 0);
            this.tableLayoutPanel18.Controls.Add(this.btnRawGPSCounterReset, 2, 0);
            this.tableLayoutPanel18.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLayoutPanel18.Location = new System.Drawing.Point(3, 155);
            this.tableLayoutPanel18.Name = "tableLayoutPanel18";
            this.tableLayoutPanel18.RowCount = 1;
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.Size = new System.Drawing.Size(356, 30);
            this.tableLayoutPanel18.TabIndex = 6;
            // 
            // lblRawGPSCounterTotal
            // 
            this.lblRawGPSCounterTotal.AutoSize = true;
            this.lblRawGPSCounterTotal.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblRawGPSCounterTotal.Location = new System.Drawing.Point(3, 0);
            this.lblRawGPSCounterTotal.Name = "lblRawGPSCounterTotal";
            this.lblRawGPSCounterTotal.Size = new System.Drawing.Size(126, 30);
            this.lblRawGPSCounterTotal.TabIndex = 0;
            this.lblRawGPSCounterTotal.Text = "Количество сообщений";
            // 
            // lblRawGPSCounterError
            // 
            this.lblRawGPSCounterError.AutoSize = true;
            this.lblRawGPSCounterError.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblRawGPSCounterError.Location = new System.Drawing.Point(144, 0);
            this.lblRawGPSCounterError.Name = "lblRawGPSCounterError";
            this.lblRawGPSCounterError.Size = new System.Drawing.Size(91, 30);
            this.lblRawGPSCounterError.TabIndex = 1;
            this.lblRawGPSCounterError.Text = "Из них потеряно";
            // 
            // btnRawGPSCounterReset
            // 
            this.btnRawGPSCounterReset.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnRawGPSCounterReset.Location = new System.Drawing.Point(265, 3);
            this.btnRawGPSCounterReset.Name = "btnRawGPSCounterReset";
            this.btnRawGPSCounterReset.Size = new System.Drawing.Size(75, 24);
            this.btnRawGPSCounterReset.TabIndex = 2;
            this.btnRawGPSCounterReset.Text = "Сброс";
            this.btnRawGPSCounterReset.UseVisualStyleBackColor = true;
            this.btnRawGPSCounterReset.Click += new System.EventHandler(this.btnRawGPSCounterReset_Click);
            // 
            // txbReceiveGPSData
            // 
            this.txbReceiveGPSData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txbReceiveGPSData.Location = new System.Drawing.Point(3, 38);
            this.txbReceiveGPSData.MaxLength = 100;
            this.txbReceiveGPSData.Name = "txbReceiveGPSData";
            this.txbReceiveGPSData.Size = new System.Drawing.Size(615, 111);
            this.txbReceiveGPSData.TabIndex = 2;
            this.txbReceiveGPSData.Text = "";
            // 
            // tableLayoutPanel17
            // 
            this.tableLayoutPanel17.ColumnCount = 4;
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 145F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 82F));
            this.tableLayoutPanel17.Controls.Add(this.chbxRawGPSVeiwNumber, 0, 0);
            this.tableLayoutPanel17.Controls.Add(this.chbxRawGPSVeiwTime, 1, 0);
            this.tableLayoutPanel17.Controls.Add(this.chbxRawGPSVeiwData, 2, 0);
            this.tableLayoutPanel17.Controls.Add(this.btnRawGPSClearn, 3, 0);
            this.tableLayoutPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel17.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel17.Name = "tableLayoutPanel17";
            this.tableLayoutPanel17.RowCount = 1;
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel17.Size = new System.Drawing.Size(615, 29);
            this.tableLayoutPanel17.TabIndex = 5;
            // 
            // chbxRawGPSVeiwNumber
            // 
            this.chbxRawGPSVeiwNumber.AutoSize = true;
            this.chbxRawGPSVeiwNumber.Checked = true;
            this.chbxRawGPSVeiwNumber.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbxRawGPSVeiwNumber.Dock = System.Windows.Forms.DockStyle.Left;
            this.chbxRawGPSVeiwNumber.Location = new System.Drawing.Point(3, 3);
            this.chbxRawGPSVeiwNumber.Name = "chbxRawGPSVeiwNumber";
            this.chbxRawGPSVeiwNumber.Size = new System.Drawing.Size(98, 23);
            this.chbxRawGPSVeiwNumber.TabIndex = 0;
            this.chbxRawGPSVeiwNumber.Text = "Номер пакета";
            this.chbxRawGPSVeiwNumber.UseVisualStyleBackColor = true;
            // 
            // chbxRawGPSVeiwTime
            // 
            this.chbxRawGPSVeiwTime.AutoSize = true;
            this.chbxRawGPSVeiwTime.Checked = true;
            this.chbxRawGPSVeiwTime.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbxRawGPSVeiwTime.Dock = System.Windows.Forms.DockStyle.Left;
            this.chbxRawGPSVeiwTime.Location = new System.Drawing.Point(123, 3);
            this.chbxRawGPSVeiwTime.Name = "chbxRawGPSVeiwTime";
            this.chbxRawGPSVeiwTime.Size = new System.Drawing.Size(127, 23);
            this.chbxRawGPSVeiwTime.TabIndex = 1;
            this.chbxRawGPSVeiwTime.Text = "Время отправления";
            this.chbxRawGPSVeiwTime.UseVisualStyleBackColor = true;
            // 
            // chbxRawGPSVeiwData
            // 
            this.chbxRawGPSVeiwData.AutoSize = true;
            this.chbxRawGPSVeiwData.Checked = true;
            this.chbxRawGPSVeiwData.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbxRawGPSVeiwData.Dock = System.Windows.Forms.DockStyle.Left;
            this.chbxRawGPSVeiwData.Location = new System.Drawing.Point(268, 3);
            this.chbxRawGPSVeiwData.Name = "chbxRawGPSVeiwData";
            this.chbxRawGPSVeiwData.Size = new System.Drawing.Size(67, 23);
            this.chbxRawGPSVeiwData.TabIndex = 1;
            this.chbxRawGPSVeiwData.Text = "Данные";
            this.chbxRawGPSVeiwData.UseVisualStyleBackColor = true;
            // 
            // btnRawGPSClearn
            // 
            this.btnRawGPSClearn.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnRawGPSClearn.Location = new System.Drawing.Point(537, 3);
            this.btnRawGPSClearn.Name = "btnRawGPSClearn";
            this.btnRawGPSClearn.Size = new System.Drawing.Size(75, 23);
            this.btnRawGPSClearn.TabIndex = 2;
            this.btnRawGPSClearn.Text = "Очистить";
            this.btnRawGPSClearn.UseVisualStyleBackColor = true;
            this.btnRawGPSClearn.Click += new System.EventHandler(this.btnRawGPSClearn_Click);
            // 
            // gbxTransmiteName
            // 
            this.gbxTransmiteName.Controls.Add(this.tableLayoutPanel2);
            this.gbxTransmiteName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbxTransmiteName.Location = new System.Drawing.Point(3, 646);
            this.gbxTransmiteName.Name = "gbxTransmiteName";
            this.gbxTransmiteName.Size = new System.Drawing.Size(627, 52);
            this.gbxTransmiteName.TabIndex = 7;
            this.gbxTransmiteName.TabStop = false;
            this.gbxTransmiteName.Text = "Исходящие команды GPS";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel2.Controls.Add(this.txbTransmiteData, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnTransmiteData, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(621, 33);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // txbTransmiteData
            // 
            this.txbTransmiteData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txbTransmiteData.Location = new System.Drawing.Point(3, 3);
            this.txbTransmiteData.Name = "txbTransmiteData";
            this.txbTransmiteData.Size = new System.Drawing.Size(495, 27);
            this.txbTransmiteData.TabIndex = 2;
            this.txbTransmiteData.Text = "";
            // 
            // btnTransmiteData
            // 
            this.btnTransmiteData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTransmiteData.Location = new System.Drawing.Point(504, 3);
            this.btnTransmiteData.Name = "btnTransmiteData";
            this.btnTransmiteData.Size = new System.Drawing.Size(114, 27);
            this.btnTransmiteData.TabIndex = 3;
            this.btnTransmiteData.Text = "btnTransmiteData";
            this.btnTransmiteData.UseVisualStyleBackColor = true;
            this.btnTransmiteData.Click += new System.EventHandler(this.btnTransmiteData_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuConnect,
            this.menuDisconnect});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1189, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuConnect
            // 
            this.menuConnect.AutoSize = false;
            this.menuConnect.Name = "menuConnect";
            this.menuConnect.Size = new System.Drawing.Size(95, 20);
            this.menuConnect.Text = global::UPDConsole.Resources.MenuConnect;
            this.menuConnect.Click += new System.EventHandler(this.menuConnect_Click);
            // 
            // menuDisconnect
            // 
            this.menuDisconnect.Enabled = false;
            this.menuDisconnect.Name = "menuDisconnect";
            this.menuDisconnect.Size = new System.Drawing.Size(87, 20);
            this.menuDisconnect.Text = global::UPDConsole.Resources.MenuDisconnect;
            this.menuDisconnect.Click += new System.EventHandler(this.menuDisconnect_Click);
            // 
            // tabData
            // 
            this.tabData.Controls.Add(this.tabRAWDataPage);
            this.tabData.Controls.Add(this.tabCalDataPage);
            this.tabData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabData.Location = new System.Drawing.Point(0, 24);
            this.tabData.Name = "tabData";
            this.tabData.SelectedIndex = 0;
            this.tabData.Size = new System.Drawing.Size(1189, 739);
            this.tabData.TabIndex = 3;
            // 
            // tabRAWDataPage
            // 
            this.tabRAWDataPage.Controls.Add(this.tableLayoutPanel21);
            this.tabRAWDataPage.Location = new System.Drawing.Point(4, 22);
            this.tabRAWDataPage.Name = "tabRAWDataPage";
            this.tabRAWDataPage.Padding = new System.Windows.Forms.Padding(3);
            this.tabRAWDataPage.Size = new System.Drawing.Size(1181, 713);
            this.tabRAWDataPage.TabIndex = 0;
            this.tabRAWDataPage.Text = global::UPDConsole.Resources.TabRAWDataPage;
            this.tabRAWDataPage.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel21
            // 
            this.tableLayoutPanel21.ColumnCount = 2;
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 536F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel21.Controls.Add(this.tableLayoutPanel4, 0, 0);
            this.tableLayoutPanel21.Controls.Add(this.tableLayoutPanel1, 1, 0);
            this.tableLayoutPanel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel21.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel21.Name = "tableLayoutPanel21";
            this.tableLayoutPanel21.RowCount = 1;
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel21.Size = new System.Drawing.Size(1175, 707);
            this.tableLayoutPanel21.TabIndex = 0;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.grbxMessage, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel12, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.grbxMessageList, 0, 2);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 3;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 42.20472F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.354331F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 52.59843F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(530, 701);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // grbxMessage
            // 
            this.grbxMessage.Controls.Add(this.tableLayoutPanel7);
            this.grbxMessage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbxMessage.Location = new System.Drawing.Point(3, 3);
            this.grbxMessage.Name = "grbxMessage";
            this.grbxMessage.Size = new System.Drawing.Size(524, 289);
            this.grbxMessage.TabIndex = 0;
            this.grbxMessage.TabStop = false;
            this.grbxMessage.Text = "Сообщение";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 3;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 122F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Controls.Add(this.rbtnEmpty, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.rbtnConfigRegister, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel11, 2, 4);
            this.tableLayoutPanel7.Controls.Add(this.rbtnControlFSMC, 0, 2);
            this.tableLayoutPanel7.Controls.Add(this.rbtnControlGPS, 0, 3);
            this.tableLayoutPanel7.Controls.Add(this.pnlMessageEmpty, 2, 0);
            this.tableLayoutPanel7.Controls.Add(this.pnlMessageConfigRegister, 2, 1);
            this.tableLayoutPanel7.Controls.Add(this.pnlMessageControlFSMC, 2, 2);
            this.tableLayoutPanel7.Controls.Add(this.pnlMessageControlGPS, 2, 3);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 5;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(518, 270);
            this.tableLayoutPanel7.TabIndex = 0;
            // 
            // rbtnEmpty
            // 
            this.rbtnEmpty.AutoSize = true;
            this.rbtnEmpty.Checked = true;
            this.tableLayoutPanel7.SetColumnSpan(this.rbtnEmpty, 2);
            this.rbtnEmpty.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbtnEmpty.Location = new System.Drawing.Point(3, 3);
            this.rbtnEmpty.Name = "rbtnEmpty";
            this.rbtnEmpty.Size = new System.Drawing.Size(61, 23);
            this.rbtnEmpty.TabIndex = 0;
            this.rbtnEmpty.TabStop = true;
            this.rbtnEmpty.Text = "Пустой";
            this.rbtnEmpty.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.rbtnEmpty.UseVisualStyleBackColor = true;
            this.rbtnEmpty.CheckedChanged += new System.EventHandler(this.rbtnMessageType_CheckedChanged);
            // 
            // rbtnConfigRegister
            // 
            this.rbtnConfigRegister.AutoSize = true;
            this.tableLayoutPanel7.SetColumnSpan(this.rbtnConfigRegister, 2);
            this.rbtnConfigRegister.Dock = System.Windows.Forms.DockStyle.Top;
            this.rbtnConfigRegister.Location = new System.Drawing.Point(3, 32);
            this.rbtnConfigRegister.Name = "rbtnConfigRegister";
            this.rbtnConfigRegister.Size = new System.Drawing.Size(141, 17);
            this.rbtnConfigRegister.TabIndex = 1;
            this.rbtnConfigRegister.Text = "Конфигурация FSMC";
            this.rbtnConfigRegister.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.rbtnConfigRegister.UseVisualStyleBackColor = true;
            this.rbtnConfigRegister.CheckedChanged += new System.EventHandler(this.rbtnMessageType_CheckedChanged);
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 3;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 52.58964F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 47.41036F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 114F));
            this.tableLayoutPanel11.Controls.Add(this.btnApplyMessage, 2, 0);
            this.tableLayoutPanel11.Controls.Add(this.numDelay, 1, 0);
            this.tableLayoutPanel11.Controls.Add(this.lblDelay, 0, 0);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(150, 237);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 1;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(355, 30);
            this.tableLayoutPanel11.TabIndex = 8;
            // 
            // btnApplyMessage
            // 
            this.btnApplyMessage.Location = new System.Drawing.Point(243, 3);
            this.btnApplyMessage.Name = "btnApplyMessage";
            this.btnApplyMessage.Size = new System.Drawing.Size(98, 21);
            this.btnApplyMessage.TabIndex = 7;
            this.btnApplyMessage.Text = "Применить";
            this.btnApplyMessage.UseVisualStyleBackColor = true;
            this.btnApplyMessage.Click += new System.EventHandler(this.btnApplyMessage_Click);
            // 
            // numDelay
            // 
            this.numDelay.Dock = System.Windows.Forms.DockStyle.Left;
            this.numDelay.Location = new System.Drawing.Point(129, 3);
            this.numDelay.Maximum = new decimal(new int[] {
            40000,
            0,
            0,
            0});
            this.numDelay.Name = "numDelay";
            this.numDelay.Size = new System.Drawing.Size(108, 20);
            this.numDelay.TabIndex = 8;
            // 
            // lblDelay
            // 
            this.lblDelay.AutoSize = true;
            this.lblDelay.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblDelay.Location = new System.Drawing.Point(45, 0);
            this.lblDelay.Name = "lblDelay";
            this.lblDelay.Size = new System.Drawing.Size(78, 30);
            this.lblDelay.TabIndex = 9;
            this.lblDelay.Text = "Задержка, мс";
            this.lblDelay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rbtnControlFSMC
            // 
            this.rbtnControlFSMC.AutoSize = true;
            this.tableLayoutPanel7.SetColumnSpan(this.rbtnControlFSMC, 2);
            this.rbtnControlFSMC.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbtnControlFSMC.Location = new System.Drawing.Point(3, 167);
            this.rbtnControlFSMC.Name = "rbtnControlFSMC";
            this.rbtnControlFSMC.Size = new System.Drawing.Size(119, 29);
            this.rbtnControlFSMC.TabIndex = 2;
            this.rbtnControlFSMC.Text = "Управление FSMC";
            this.rbtnControlFSMC.UseVisualStyleBackColor = true;
            this.rbtnControlFSMC.CheckedChanged += new System.EventHandler(this.rbtnMessageType_CheckedChanged);
            // 
            // rbtnControlGPS
            // 
            this.rbtnControlGPS.AutoSize = true;
            this.tableLayoutPanel7.SetColumnSpan(this.rbtnControlGPS, 2);
            this.rbtnControlGPS.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbtnControlGPS.Location = new System.Drawing.Point(3, 202);
            this.rbtnControlGPS.Name = "rbtnControlGPS";
            this.rbtnControlGPS.Size = new System.Drawing.Size(112, 29);
            this.rbtnControlGPS.TabIndex = 2;
            this.rbtnControlGPS.Text = "Управление GPS";
            this.rbtnControlGPS.UseVisualStyleBackColor = true;
            this.rbtnControlGPS.CheckedChanged += new System.EventHandler(this.rbtnMessageType_CheckedChanged);
            // 
            // pnlMessageEmpty
            // 
            this.pnlMessageEmpty.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMessageEmpty.Location = new System.Drawing.Point(150, 3);
            this.pnlMessageEmpty.Name = "pnlMessageEmpty";
            this.pnlMessageEmpty.Size = new System.Drawing.Size(365, 23);
            this.pnlMessageEmpty.TabIndex = 6;
            // 
            // pnlMessageConfigRegister
            // 
            this.pnlMessageConfigRegister.Controls.Add(this.tableLayoutPanel10);
            this.pnlMessageConfigRegister.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMessageConfigRegister.Enabled = false;
            this.pnlMessageConfigRegister.Location = new System.Drawing.Point(150, 32);
            this.pnlMessageConfigRegister.Name = "pnlMessageConfigRegister";
            this.pnlMessageConfigRegister.Size = new System.Drawing.Size(365, 129);
            this.pnlMessageConfigRegister.TabIndex = 3;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 2;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 52.58964F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 47.41036F));
            this.tableLayoutPanel10.Controls.Add(this.chbxBitTest, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.chbxBitTestMode, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.chbxBitTxOn, 0, 2);
            this.tableLayoutPanel10.Controls.Add(this.chbxBitEnACQ, 0, 3);
            this.tableLayoutPanel10.Controls.Add(this.lblFrequncyName, 1, 0);
            this.tableLayoutPanel10.Controls.Add(this.numBitsFrq, 1, 1);
            this.tableLayoutPanel10.Controls.Add(this.chbxBitManual, 1, 3);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 4;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(251, 129);
            this.tableLayoutPanel10.TabIndex = 1;
            // 
            // chbxBitTest
            // 
            this.chbxBitTest.AutoSize = true;
            this.chbxBitTest.Dock = System.Windows.Forms.DockStyle.Left;
            this.chbxBitTest.Location = new System.Drawing.Point(3, 3);
            this.chbxBitTest.Name = "chbxBitTest";
            this.chbxBitTest.Size = new System.Drawing.Size(69, 26);
            this.chbxBitTest.TabIndex = 0;
            this.chbxBitTest.Text = "TEST [0]";
            this.chbxBitTest.UseVisualStyleBackColor = true;
            // 
            // chbxBitTestMode
            // 
            this.chbxBitTestMode.AutoSize = true;
            this.chbxBitTestMode.Dock = System.Windows.Forms.DockStyle.Left;
            this.chbxBitTestMode.Location = new System.Drawing.Point(3, 35);
            this.chbxBitTestMode.Name = "chbxBitTestMode";
            this.chbxBitTestMode.Size = new System.Drawing.Size(107, 26);
            this.chbxBitTestMode.TabIndex = 1;
            this.chbxBitTestMode.Text = "TEST_MODE [1]";
            this.chbxBitTestMode.UseVisualStyleBackColor = true;
            // 
            // chbxBitTxOn
            // 
            this.chbxBitTxOn.AutoSize = true;
            this.chbxBitTxOn.Dock = System.Windows.Forms.DockStyle.Left;
            this.chbxBitTxOn.Location = new System.Drawing.Point(3, 67);
            this.chbxBitTxOn.Name = "chbxBitTxOn";
            this.chbxBitTxOn.Size = new System.Drawing.Size(77, 26);
            this.chbxBitTxOn.TabIndex = 2;
            this.chbxBitTxOn.Text = "TX_ON [4]";
            this.chbxBitTxOn.UseVisualStyleBackColor = true;
            // 
            // chbxBitEnACQ
            // 
            this.chbxBitEnACQ.AutoSize = true;
            this.chbxBitEnACQ.Dock = System.Windows.Forms.DockStyle.Left;
            this.chbxBitEnACQ.Location = new System.Drawing.Point(3, 99);
            this.chbxBitEnACQ.Name = "chbxBitEnACQ";
            this.chbxBitEnACQ.Size = new System.Drawing.Size(84, 27);
            this.chbxBitEnACQ.TabIndex = 3;
            this.chbxBitEnACQ.Text = "EN_ACQ [5]";
            this.chbxBitEnACQ.UseVisualStyleBackColor = true;
            // 
            // lblFrequncyName
            // 
            this.lblFrequncyName.AutoSize = true;
            this.lblFrequncyName.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblFrequncyName.Location = new System.Drawing.Point(135, 0);
            this.lblFrequncyName.Name = "lblFrequncyName";
            this.lblFrequncyName.Size = new System.Drawing.Size(98, 32);
            this.lblFrequncyName.TabIndex = 4;
            this.lblFrequncyName.Text = "FRQ [8...13]  0...63";
            this.lblFrequncyName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // numBitsFrq
            // 
            this.numBitsFrq.Dock = System.Windows.Forms.DockStyle.Left;
            this.numBitsFrq.Location = new System.Drawing.Point(135, 35);
            this.numBitsFrq.Maximum = new decimal(new int[] {
            63,
            0,
            0,
            0});
            this.numBitsFrq.Name = "numBitsFrq";
            this.numBitsFrq.Size = new System.Drawing.Size(65, 20);
            this.numBitsFrq.TabIndex = 5;
            // 
            // chbxBitManual
            // 
            this.chbxBitManual.AutoSize = true;
            this.chbxBitManual.Dock = System.Windows.Forms.DockStyle.Left;
            this.chbxBitManual.Location = new System.Drawing.Point(135, 99);
            this.chbxBitManual.Name = "chbxBitManual";
            this.chbxBitManual.Size = new System.Drawing.Size(71, 27);
            this.chbxBitManual.TabIndex = 6;
            this.chbxBitManual.Text = "MAN [14]";
            this.chbxBitManual.UseVisualStyleBackColor = true;
            // 
            // pnlMessageControlFSMC
            // 
            this.pnlMessageControlFSMC.Controls.Add(this.tableLayoutPanel8);
            this.pnlMessageControlFSMC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMessageControlFSMC.Enabled = false;
            this.pnlMessageControlFSMC.Location = new System.Drawing.Point(150, 167);
            this.pnlMessageControlFSMC.Name = "pnlMessageControlFSMC";
            this.pnlMessageControlFSMC.Size = new System.Drawing.Size(365, 29);
            this.pnlMessageControlFSMC.TabIndex = 4;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 52.39726F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 47.60274F));
            this.tableLayoutPanel8.Controls.Add(this.rbtnFSMCOn, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.rbtnFSMCOff, 1, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(251, 29);
            this.tableLayoutPanel8.TabIndex = 1;
            // 
            // rbtnFSMCOn
            // 
            this.rbtnFSMCOn.AutoSize = true;
            this.rbtnFSMCOn.Checked = true;
            this.rbtnFSMCOn.Location = new System.Drawing.Point(3, 3);
            this.rbtnFSMCOn.Name = "rbtnFSMCOn";
            this.rbtnFSMCOn.Size = new System.Drawing.Size(75, 17);
            this.rbtnFSMCOn.TabIndex = 1;
            this.rbtnFSMCOn.TabStop = true;
            this.rbtnFSMCOn.Text = "Включено";
            this.rbtnFSMCOn.UseVisualStyleBackColor = true;
            // 
            // rbtnFSMCOff
            // 
            this.rbtnFSMCOff.AutoSize = true;
            this.rbtnFSMCOff.Location = new System.Drawing.Point(134, 3);
            this.rbtnFSMCOff.Name = "rbtnFSMCOff";
            this.rbtnFSMCOff.Size = new System.Drawing.Size(83, 17);
            this.rbtnFSMCOff.TabIndex = 2;
            this.rbtnFSMCOff.Text = "Выключено";
            this.rbtnFSMCOff.UseVisualStyleBackColor = true;
            // 
            // pnlMessageControlGPS
            // 
            this.pnlMessageControlGPS.Controls.Add(this.tableLayoutPanel9);
            this.pnlMessageControlGPS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMessageControlGPS.Enabled = false;
            this.pnlMessageControlGPS.Location = new System.Drawing.Point(150, 202);
            this.pnlMessageControlGPS.Name = "pnlMessageControlGPS";
            this.pnlMessageControlGPS.Size = new System.Drawing.Size(365, 29);
            this.pnlMessageControlGPS.TabIndex = 5;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 2;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 52.39726F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 47.60274F));
            this.tableLayoutPanel9.Controls.Add(this.rbtnGPSOn, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.rbtnGPSOff, 1, 0);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(251, 29);
            this.tableLayoutPanel9.TabIndex = 2;
            // 
            // rbtnGPSOn
            // 
            this.rbtnGPSOn.AutoSize = true;
            this.rbtnGPSOn.Checked = true;
            this.rbtnGPSOn.Location = new System.Drawing.Point(3, 3);
            this.rbtnGPSOn.Name = "rbtnGPSOn";
            this.rbtnGPSOn.Size = new System.Drawing.Size(75, 17);
            this.rbtnGPSOn.TabIndex = 1;
            this.rbtnGPSOn.TabStop = true;
            this.rbtnGPSOn.Text = "Включено";
            this.rbtnGPSOn.UseVisualStyleBackColor = true;
            // 
            // rbtnGPSOff
            // 
            this.rbtnGPSOff.AutoSize = true;
            this.rbtnGPSOff.Location = new System.Drawing.Point(134, 3);
            this.rbtnGPSOff.Name = "rbtnGPSOff";
            this.rbtnGPSOff.Size = new System.Drawing.Size(83, 17);
            this.rbtnGPSOff.TabIndex = 2;
            this.rbtnGPSOff.Text = "Выключено";
            this.rbtnGPSOff.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 4;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.53696F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.95331F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.15175F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 44.74708F));
            this.tableLayoutPanel12.Controls.Add(this.btnTaskClear, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.btnTaskStart, 1, 0);
            this.tableLayoutPanel12.Controls.Add(this.btnTaskStop, 2, 0);
            this.tableLayoutPanel12.Controls.Add(this.chbxCircylarSending, 3, 0);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(3, 298);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 1;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(524, 31);
            this.tableLayoutPanel12.TabIndex = 1;
            // 
            // btnTaskClear
            // 
            this.btnTaskClear.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnTaskClear.Location = new System.Drawing.Point(3, 3);
            this.btnTaskClear.Name = "btnTaskClear";
            this.btnTaskClear.Size = new System.Drawing.Size(75, 25);
            this.btnTaskClear.TabIndex = 0;
            this.btnTaskClear.Text = "Очистить";
            this.btnTaskClear.UseVisualStyleBackColor = true;
            // 
            // btnTaskStart
            // 
            this.btnTaskStart.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnTaskStart.Location = new System.Drawing.Point(89, 3);
            this.btnTaskStart.Name = "btnTaskStart";
            this.btnTaskStart.Size = new System.Drawing.Size(75, 25);
            this.btnTaskStart.TabIndex = 1;
            this.btnTaskStart.Text = "Выполнить";
            this.btnTaskStart.UseVisualStyleBackColor = true;
            this.btnTaskStart.Click += new System.EventHandler(this.btnTaskStart_Click);
            // 
            // btnTaskStop
            // 
            this.btnTaskStop.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnTaskStop.Location = new System.Drawing.Point(172, 3);
            this.btnTaskStop.Name = "btnTaskStop";
            this.btnTaskStop.Size = new System.Drawing.Size(75, 25);
            this.btnTaskStop.TabIndex = 2;
            this.btnTaskStop.Text = "Остановить";
            this.btnTaskStop.UseVisualStyleBackColor = true;
            this.btnTaskStop.Click += new System.EventHandler(this.btnTaskStop_Click);
            // 
            // chbxCircylarSending
            // 
            this.chbxCircylarSending.AutoSize = true;
            this.chbxCircylarSending.Dock = System.Windows.Forms.DockStyle.Left;
            this.chbxCircylarSending.Location = new System.Drawing.Point(292, 3);
            this.chbxCircylarSending.Name = "chbxCircylarSending";
            this.chbxCircylarSending.Size = new System.Drawing.Size(143, 25);
            this.chbxCircylarSending.TabIndex = 3;
            this.chbxCircylarSending.Text = "Циклическая передача";
            this.chbxCircylarSending.UseVisualStyleBackColor = true;
            // 
            // grbxMessageList
            // 
            this.grbxMessageList.Controls.Add(this.tblMessageList);
            this.grbxMessageList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbxMessageList.Location = new System.Drawing.Point(3, 335);
            this.grbxMessageList.Name = "grbxMessageList";
            this.grbxMessageList.Size = new System.Drawing.Size(524, 363);
            this.grbxMessageList.TabIndex = 2;
            this.grbxMessageList.TabStop = false;
            this.grbxMessageList.Text = "Список сообщений";
            // 
            // tblMessageList
            // 
            this.tblMessageList.ColumnCount = 4;
            this.tblMessageList.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.92308F));
            this.tblMessageList.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 56.41026F));
            this.tblMessageList.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.78133F));
            this.tblMessageList.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 122F));
            this.tblMessageList.Controls.Add(this.lblDelay10, 2, 9);
            this.tblMessageList.Controls.Add(this.lblDelay9, 2, 8);
            this.tblMessageList.Controls.Add(this.lblDelay8, 2, 7);
            this.tblMessageList.Controls.Add(this.lblDelay7, 2, 6);
            this.tblMessageList.Controls.Add(this.lblDelay6, 2, 5);
            this.tblMessageList.Controls.Add(this.lblDelay5, 2, 4);
            this.tblMessageList.Controls.Add(this.lblDelay4, 2, 3);
            this.tblMessageList.Controls.Add(this.lblDelay3, 2, 2);
            this.tblMessageList.Controls.Add(this.lblDelay2, 2, 1);
            this.tblMessageList.Controls.Add(this.lblDelay1, 2, 0);
            this.tblMessageList.Controls.Add(this.btnSendMessage1, 3, 0);
            this.tblMessageList.Controls.Add(this.btnSendMessage2, 3, 1);
            this.tblMessageList.Controls.Add(this.btnSendMessage3, 3, 2);
            this.tblMessageList.Controls.Add(this.btnSendMessage4, 3, 3);
            this.tblMessageList.Controls.Add(this.btnSendMessage5, 3, 4);
            this.tblMessageList.Controls.Add(this.btnSendMessage6, 3, 5);
            this.tblMessageList.Controls.Add(this.btnSendMessage7, 3, 6);
            this.tblMessageList.Controls.Add(this.btnSendMessage8, 3, 7);
            this.tblMessageList.Controls.Add(this.btnSendMessage9, 3, 8);
            this.tblMessageList.Controls.Add(this.btnSendMessage10, 3, 9);
            this.tblMessageList.Controls.Add(this.lblMessage1, 1, 0);
            this.tblMessageList.Controls.Add(this.lblMessage2, 1, 1);
            this.tblMessageList.Controls.Add(this.lblMessage3, 1, 2);
            this.tblMessageList.Controls.Add(this.lblMessage4, 1, 3);
            this.tblMessageList.Controls.Add(this.lblMessage5, 1, 4);
            this.tblMessageList.Controls.Add(this.lblMessage6, 1, 5);
            this.tblMessageList.Controls.Add(this.lblMessage7, 1, 6);
            this.tblMessageList.Controls.Add(this.lblMessage8, 1, 7);
            this.tblMessageList.Controls.Add(this.lblMessage9, 1, 8);
            this.tblMessageList.Controls.Add(this.lblMessage10, 1, 9);
            this.tblMessageList.Controls.Add(this.rbtnMessage1, 0, 0);
            this.tblMessageList.Controls.Add(this.rbtnMessage2, 0, 1);
            this.tblMessageList.Controls.Add(this.rbtnMessage3, 0, 2);
            this.tblMessageList.Controls.Add(this.rbtnMessage4, 0, 3);
            this.tblMessageList.Controls.Add(this.rbtnMessage5, 0, 4);
            this.tblMessageList.Controls.Add(this.rbtnMessage6, 0, 5);
            this.tblMessageList.Controls.Add(this.rbtnMessage7, 0, 6);
            this.tblMessageList.Controls.Add(this.rbtnMessage8, 0, 7);
            this.tblMessageList.Controls.Add(this.rbtnMessage9, 0, 8);
            this.tblMessageList.Controls.Add(this.rbtnMessage10, 0, 9);
            this.tblMessageList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblMessageList.Location = new System.Drawing.Point(3, 16);
            this.tblMessageList.Name = "tblMessageList";
            this.tblMessageList.RowCount = 10;
            this.tblMessageList.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tblMessageList.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tblMessageList.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tblMessageList.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tblMessageList.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tblMessageList.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tblMessageList.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tblMessageList.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tblMessageList.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tblMessageList.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tblMessageList.Size = new System.Drawing.Size(518, 344);
            this.tblMessageList.TabIndex = 2;
            // 
            // lblDelay10
            // 
            this.lblDelay10.AutoSize = true;
            this.lblDelay10.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblDelay10.Location = new System.Drawing.Point(292, 306);
            this.lblDelay10.Name = "lblDelay10";
            this.lblDelay10.Size = new System.Drawing.Size(83, 38);
            this.lblDelay10.TabIndex = 11;
            this.lblDelay10.Text = "Не передается";
            this.lblDelay10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDelay9
            // 
            this.lblDelay9.AutoSize = true;
            this.lblDelay9.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblDelay9.Location = new System.Drawing.Point(292, 272);
            this.lblDelay9.Name = "lblDelay9";
            this.lblDelay9.Size = new System.Drawing.Size(83, 34);
            this.lblDelay9.TabIndex = 10;
            this.lblDelay9.Text = "Не передается";
            this.lblDelay9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDelay8
            // 
            this.lblDelay8.AutoSize = true;
            this.lblDelay8.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblDelay8.Location = new System.Drawing.Point(292, 238);
            this.lblDelay8.Name = "lblDelay8";
            this.lblDelay8.Size = new System.Drawing.Size(83, 34);
            this.lblDelay8.TabIndex = 9;
            this.lblDelay8.Text = "Не передается";
            this.lblDelay8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDelay7
            // 
            this.lblDelay7.AutoSize = true;
            this.lblDelay7.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblDelay7.Location = new System.Drawing.Point(292, 204);
            this.lblDelay7.Name = "lblDelay7";
            this.lblDelay7.Size = new System.Drawing.Size(83, 34);
            this.lblDelay7.TabIndex = 8;
            this.lblDelay7.Text = "Не передается";
            this.lblDelay7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDelay6
            // 
            this.lblDelay6.AutoSize = true;
            this.lblDelay6.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblDelay6.Location = new System.Drawing.Point(292, 170);
            this.lblDelay6.Name = "lblDelay6";
            this.lblDelay6.Size = new System.Drawing.Size(83, 34);
            this.lblDelay6.TabIndex = 7;
            this.lblDelay6.Text = "Не передается";
            this.lblDelay6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDelay5
            // 
            this.lblDelay5.AutoSize = true;
            this.lblDelay5.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblDelay5.Location = new System.Drawing.Point(292, 136);
            this.lblDelay5.Name = "lblDelay5";
            this.lblDelay5.Size = new System.Drawing.Size(83, 34);
            this.lblDelay5.TabIndex = 6;
            this.lblDelay5.Text = "Не передается";
            this.lblDelay5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDelay4
            // 
            this.lblDelay4.AutoSize = true;
            this.lblDelay4.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblDelay4.Location = new System.Drawing.Point(292, 102);
            this.lblDelay4.Name = "lblDelay4";
            this.lblDelay4.Size = new System.Drawing.Size(83, 34);
            this.lblDelay4.TabIndex = 5;
            this.lblDelay4.Text = "Не передается";
            this.lblDelay4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDelay3
            // 
            this.lblDelay3.AutoSize = true;
            this.lblDelay3.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblDelay3.Location = new System.Drawing.Point(292, 68);
            this.lblDelay3.Name = "lblDelay3";
            this.lblDelay3.Size = new System.Drawing.Size(83, 34);
            this.lblDelay3.TabIndex = 4;
            this.lblDelay3.Text = "Не передается";
            this.lblDelay3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDelay2
            // 
            this.lblDelay2.AutoSize = true;
            this.lblDelay2.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblDelay2.Location = new System.Drawing.Point(292, 34);
            this.lblDelay2.Name = "lblDelay2";
            this.lblDelay2.Size = new System.Drawing.Size(83, 34);
            this.lblDelay2.TabIndex = 3;
            this.lblDelay2.Text = "Не передается";
            this.lblDelay2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDelay1
            // 
            this.lblDelay1.AutoSize = true;
            this.lblDelay1.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblDelay1.Location = new System.Drawing.Point(292, 0);
            this.lblDelay1.Name = "lblDelay1";
            this.lblDelay1.Size = new System.Drawing.Size(83, 34);
            this.lblDelay1.TabIndex = 2;
            this.lblDelay1.Text = "Не передается";
            this.lblDelay1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnSendMessage1
            // 
            this.btnSendMessage1.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSendMessage1.ForeColor = System.Drawing.Color.Black;
            this.btnSendMessage1.Location = new System.Drawing.Point(420, 3);
            this.btnSendMessage1.Name = "btnSendMessage1";
            this.btnSendMessage1.Size = new System.Drawing.Size(95, 28);
            this.btnSendMessage1.TabIndex = 0;
            this.btnSendMessage1.Text = "Отправить №1";
            this.btnSendMessage1.UseVisualStyleBackColor = true;
            this.btnSendMessage1.Click += new System.EventHandler(this.btnSendMessage_Click);
            // 
            // btnSendMessage2
            // 
            this.btnSendMessage2.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSendMessage2.Location = new System.Drawing.Point(420, 37);
            this.btnSendMessage2.Name = "btnSendMessage2";
            this.btnSendMessage2.Size = new System.Drawing.Size(95, 28);
            this.btnSendMessage2.TabIndex = 0;
            this.btnSendMessage2.Text = "Отправить №2";
            this.btnSendMessage2.UseVisualStyleBackColor = true;
            this.btnSendMessage2.Click += new System.EventHandler(this.btnSendMessage_Click);
            // 
            // btnSendMessage3
            // 
            this.btnSendMessage3.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSendMessage3.Location = new System.Drawing.Point(420, 71);
            this.btnSendMessage3.Name = "btnSendMessage3";
            this.btnSendMessage3.Size = new System.Drawing.Size(95, 28);
            this.btnSendMessage3.TabIndex = 0;
            this.btnSendMessage3.Text = "Отправить №3";
            this.btnSendMessage3.UseVisualStyleBackColor = true;
            this.btnSendMessage3.Click += new System.EventHandler(this.btnSendMessage_Click);
            // 
            // btnSendMessage4
            // 
            this.btnSendMessage4.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSendMessage4.Location = new System.Drawing.Point(420, 105);
            this.btnSendMessage4.Name = "btnSendMessage4";
            this.btnSendMessage4.Size = new System.Drawing.Size(95, 28);
            this.btnSendMessage4.TabIndex = 0;
            this.btnSendMessage4.Text = "Отправить №4";
            this.btnSendMessage4.UseVisualStyleBackColor = true;
            this.btnSendMessage4.Click += new System.EventHandler(this.btnSendMessage_Click);
            // 
            // btnSendMessage5
            // 
            this.btnSendMessage5.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSendMessage5.Location = new System.Drawing.Point(420, 139);
            this.btnSendMessage5.Name = "btnSendMessage5";
            this.btnSendMessage5.Size = new System.Drawing.Size(95, 28);
            this.btnSendMessage5.TabIndex = 0;
            this.btnSendMessage5.Text = "Отправить №5";
            this.btnSendMessage5.UseVisualStyleBackColor = true;
            this.btnSendMessage5.Click += new System.EventHandler(this.btnSendMessage_Click);
            // 
            // btnSendMessage6
            // 
            this.btnSendMessage6.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSendMessage6.Location = new System.Drawing.Point(420, 173);
            this.btnSendMessage6.Name = "btnSendMessage6";
            this.btnSendMessage6.Size = new System.Drawing.Size(95, 28);
            this.btnSendMessage6.TabIndex = 0;
            this.btnSendMessage6.Text = "Отправить №6";
            this.btnSendMessage6.UseVisualStyleBackColor = true;
            this.btnSendMessage6.Click += new System.EventHandler(this.btnSendMessage_Click);
            // 
            // btnSendMessage7
            // 
            this.btnSendMessage7.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSendMessage7.Location = new System.Drawing.Point(420, 207);
            this.btnSendMessage7.Name = "btnSendMessage7";
            this.btnSendMessage7.Size = new System.Drawing.Size(95, 28);
            this.btnSendMessage7.TabIndex = 0;
            this.btnSendMessage7.Text = "Отправить №7";
            this.btnSendMessage7.UseVisualStyleBackColor = true;
            this.btnSendMessage7.Click += new System.EventHandler(this.btnSendMessage_Click);
            // 
            // btnSendMessage8
            // 
            this.btnSendMessage8.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSendMessage8.Location = new System.Drawing.Point(420, 241);
            this.btnSendMessage8.Name = "btnSendMessage8";
            this.btnSendMessage8.Size = new System.Drawing.Size(95, 28);
            this.btnSendMessage8.TabIndex = 0;
            this.btnSendMessage8.Text = "Отправить №8";
            this.btnSendMessage8.UseVisualStyleBackColor = true;
            this.btnSendMessage8.Click += new System.EventHandler(this.btnSendMessage_Click);
            // 
            // btnSendMessage9
            // 
            this.btnSendMessage9.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSendMessage9.Location = new System.Drawing.Point(420, 275);
            this.btnSendMessage9.Name = "btnSendMessage9";
            this.btnSendMessage9.Size = new System.Drawing.Size(95, 28);
            this.btnSendMessage9.TabIndex = 0;
            this.btnSendMessage9.Text = "Отправить №9";
            this.btnSendMessage9.UseVisualStyleBackColor = true;
            this.btnSendMessage9.Click += new System.EventHandler(this.btnSendMessage_Click);
            // 
            // btnSendMessage10
            // 
            this.btnSendMessage10.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSendMessage10.Location = new System.Drawing.Point(420, 309);
            this.btnSendMessage10.Name = "btnSendMessage10";
            this.btnSendMessage10.Size = new System.Drawing.Size(95, 32);
            this.btnSendMessage10.TabIndex = 0;
            this.btnSendMessage10.Text = "Отправить №10";
            this.btnSendMessage10.UseVisualStyleBackColor = true;
            this.btnSendMessage10.Click += new System.EventHandler(this.btnSendMessage_Click);
            // 
            // lblMessage1
            // 
            this.lblMessage1.AutoSize = true;
            this.lblMessage1.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblMessage1.Location = new System.Drawing.Point(69, 0);
            this.lblMessage1.Name = "lblMessage1";
            this.lblMessage1.Size = new System.Drawing.Size(86, 34);
            this.lblMessage1.TabIndex = 1;
            this.lblMessage1.Text = "Нет сообщения";
            this.lblMessage1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMessage2
            // 
            this.lblMessage2.AutoSize = true;
            this.lblMessage2.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblMessage2.Location = new System.Drawing.Point(69, 34);
            this.lblMessage2.Name = "lblMessage2";
            this.lblMessage2.Size = new System.Drawing.Size(86, 34);
            this.lblMessage2.TabIndex = 1;
            this.lblMessage2.Text = "Нет сообщения";
            this.lblMessage2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMessage3
            // 
            this.lblMessage3.AutoSize = true;
            this.lblMessage3.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblMessage3.Location = new System.Drawing.Point(69, 68);
            this.lblMessage3.Name = "lblMessage3";
            this.lblMessage3.Size = new System.Drawing.Size(86, 34);
            this.lblMessage3.TabIndex = 1;
            this.lblMessage3.Text = "Нет сообщения";
            this.lblMessage3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMessage4
            // 
            this.lblMessage4.AutoSize = true;
            this.lblMessage4.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblMessage4.Location = new System.Drawing.Point(69, 102);
            this.lblMessage4.Name = "lblMessage4";
            this.lblMessage4.Size = new System.Drawing.Size(86, 34);
            this.lblMessage4.TabIndex = 1;
            this.lblMessage4.Text = "Нет сообщения";
            this.lblMessage4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMessage5
            // 
            this.lblMessage5.AutoSize = true;
            this.lblMessage5.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblMessage5.Location = new System.Drawing.Point(69, 136);
            this.lblMessage5.Name = "lblMessage5";
            this.lblMessage5.Size = new System.Drawing.Size(86, 34);
            this.lblMessage5.TabIndex = 1;
            this.lblMessage5.Text = "Нет сообщения";
            this.lblMessage5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMessage6
            // 
            this.lblMessage6.AutoSize = true;
            this.lblMessage6.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblMessage6.Location = new System.Drawing.Point(69, 170);
            this.lblMessage6.Name = "lblMessage6";
            this.lblMessage6.Size = new System.Drawing.Size(86, 34);
            this.lblMessage6.TabIndex = 1;
            this.lblMessage6.Text = "Нет сообщения";
            this.lblMessage6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMessage7
            // 
            this.lblMessage7.AutoSize = true;
            this.lblMessage7.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblMessage7.Location = new System.Drawing.Point(69, 204);
            this.lblMessage7.Name = "lblMessage7";
            this.lblMessage7.Size = new System.Drawing.Size(86, 34);
            this.lblMessage7.TabIndex = 1;
            this.lblMessage7.Text = "Нет сообщения";
            this.lblMessage7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMessage8
            // 
            this.lblMessage8.AutoSize = true;
            this.lblMessage8.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblMessage8.Location = new System.Drawing.Point(69, 238);
            this.lblMessage8.Name = "lblMessage8";
            this.lblMessage8.Size = new System.Drawing.Size(86, 34);
            this.lblMessage8.TabIndex = 1;
            this.lblMessage8.Text = "Нет сообщения";
            this.lblMessage8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMessage9
            // 
            this.lblMessage9.AutoSize = true;
            this.lblMessage9.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblMessage9.Location = new System.Drawing.Point(69, 272);
            this.lblMessage9.Name = "lblMessage9";
            this.lblMessage9.Size = new System.Drawing.Size(86, 34);
            this.lblMessage9.TabIndex = 1;
            this.lblMessage9.Text = "Нет сообщения";
            this.lblMessage9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMessage10
            // 
            this.lblMessage10.AutoSize = true;
            this.lblMessage10.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblMessage10.Location = new System.Drawing.Point(69, 306);
            this.lblMessage10.Name = "lblMessage10";
            this.lblMessage10.Size = new System.Drawing.Size(86, 38);
            this.lblMessage10.TabIndex = 1;
            this.lblMessage10.Text = "Нет сообщения";
            this.lblMessage10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rbtnMessage1
            // 
            this.rbtnMessage1.AutoSize = true;
            this.rbtnMessage1.Checked = true;
            this.rbtnMessage1.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbtnMessage1.ForeColor = System.Drawing.Color.Black;
            this.rbtnMessage1.Location = new System.Drawing.Point(3, 3);
            this.rbtnMessage1.Name = "rbtnMessage1";
            this.rbtnMessage1.Size = new System.Drawing.Size(42, 28);
            this.rbtnMessage1.TabIndex = 12;
            this.rbtnMessage1.TabStop = true;
            this.rbtnMessage1.Text = "№1";
            this.rbtnMessage1.UseVisualStyleBackColor = true;
            this.rbtnMessage1.CheckedChanged += new System.EventHandler(this.rbtnMessage_CheckedChanged);
            // 
            // rbtnMessage2
            // 
            this.rbtnMessage2.AutoSize = true;
            this.rbtnMessage2.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbtnMessage2.Location = new System.Drawing.Point(3, 37);
            this.rbtnMessage2.Name = "rbtnMessage2";
            this.rbtnMessage2.Size = new System.Drawing.Size(42, 28);
            this.rbtnMessage2.TabIndex = 12;
            this.rbtnMessage2.Text = "№2";
            this.rbtnMessage2.UseVisualStyleBackColor = true;
            this.rbtnMessage2.CheckedChanged += new System.EventHandler(this.rbtnMessage_CheckedChanged);
            // 
            // rbtnMessage3
            // 
            this.rbtnMessage3.AutoSize = true;
            this.rbtnMessage3.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbtnMessage3.Location = new System.Drawing.Point(3, 71);
            this.rbtnMessage3.Name = "rbtnMessage3";
            this.rbtnMessage3.Size = new System.Drawing.Size(42, 28);
            this.rbtnMessage3.TabIndex = 12;
            this.rbtnMessage3.Text = "№3";
            this.rbtnMessage3.UseVisualStyleBackColor = true;
            this.rbtnMessage3.CheckedChanged += new System.EventHandler(this.rbtnMessage_CheckedChanged);
            // 
            // rbtnMessage4
            // 
            this.rbtnMessage4.AutoSize = true;
            this.rbtnMessage4.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbtnMessage4.Location = new System.Drawing.Point(3, 105);
            this.rbtnMessage4.Name = "rbtnMessage4";
            this.rbtnMessage4.Size = new System.Drawing.Size(42, 28);
            this.rbtnMessage4.TabIndex = 12;
            this.rbtnMessage4.Text = "№4";
            this.rbtnMessage4.UseVisualStyleBackColor = true;
            this.rbtnMessage4.CheckedChanged += new System.EventHandler(this.rbtnMessage_CheckedChanged);
            // 
            // rbtnMessage5
            // 
            this.rbtnMessage5.AutoSize = true;
            this.rbtnMessage5.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbtnMessage5.Location = new System.Drawing.Point(3, 139);
            this.rbtnMessage5.Name = "rbtnMessage5";
            this.rbtnMessage5.Size = new System.Drawing.Size(42, 28);
            this.rbtnMessage5.TabIndex = 12;
            this.rbtnMessage5.Text = "№5";
            this.rbtnMessage5.UseVisualStyleBackColor = true;
            this.rbtnMessage5.CheckedChanged += new System.EventHandler(this.rbtnMessage_CheckedChanged);
            // 
            // rbtnMessage6
            // 
            this.rbtnMessage6.AutoSize = true;
            this.rbtnMessage6.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbtnMessage6.Location = new System.Drawing.Point(3, 173);
            this.rbtnMessage6.Name = "rbtnMessage6";
            this.rbtnMessage6.Size = new System.Drawing.Size(42, 28);
            this.rbtnMessage6.TabIndex = 12;
            this.rbtnMessage6.Text = "№6";
            this.rbtnMessage6.UseVisualStyleBackColor = true;
            this.rbtnMessage6.CheckedChanged += new System.EventHandler(this.rbtnMessage_CheckedChanged);
            // 
            // rbtnMessage7
            // 
            this.rbtnMessage7.AutoSize = true;
            this.rbtnMessage7.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbtnMessage7.Location = new System.Drawing.Point(3, 207);
            this.rbtnMessage7.Name = "rbtnMessage7";
            this.rbtnMessage7.Size = new System.Drawing.Size(42, 28);
            this.rbtnMessage7.TabIndex = 12;
            this.rbtnMessage7.Text = "№7";
            this.rbtnMessage7.UseVisualStyleBackColor = true;
            this.rbtnMessage7.CheckedChanged += new System.EventHandler(this.rbtnMessage_CheckedChanged);
            // 
            // rbtnMessage8
            // 
            this.rbtnMessage8.AutoSize = true;
            this.rbtnMessage8.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbtnMessage8.Location = new System.Drawing.Point(3, 241);
            this.rbtnMessage8.Name = "rbtnMessage8";
            this.rbtnMessage8.Size = new System.Drawing.Size(42, 28);
            this.rbtnMessage8.TabIndex = 12;
            this.rbtnMessage8.Text = "№8";
            this.rbtnMessage8.UseVisualStyleBackColor = true;
            this.rbtnMessage8.CheckedChanged += new System.EventHandler(this.rbtnMessage_CheckedChanged);
            // 
            // rbtnMessage9
            // 
            this.rbtnMessage9.AutoSize = true;
            this.rbtnMessage9.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbtnMessage9.Location = new System.Drawing.Point(3, 275);
            this.rbtnMessage9.Name = "rbtnMessage9";
            this.rbtnMessage9.Size = new System.Drawing.Size(42, 28);
            this.rbtnMessage9.TabIndex = 12;
            this.rbtnMessage9.Text = "№9";
            this.rbtnMessage9.UseVisualStyleBackColor = true;
            this.rbtnMessage9.CheckedChanged += new System.EventHandler(this.rbtnMessage_CheckedChanged);
            // 
            // rbtnMessage10
            // 
            this.rbtnMessage10.AutoSize = true;
            this.rbtnMessage10.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbtnMessage10.Location = new System.Drawing.Point(3, 309);
            this.rbtnMessage10.Name = "rbtnMessage10";
            this.rbtnMessage10.Size = new System.Drawing.Size(48, 32);
            this.rbtnMessage10.TabIndex = 12;
            this.rbtnMessage10.Text = "№10";
            this.rbtnMessage10.UseVisualStyleBackColor = true;
            this.rbtnMessage10.CheckedChanged += new System.EventHandler(this.rbtnMessage_CheckedChanged);
            // 
            // tabCalDataPage
            // 
            this.tabCalDataPage.Controls.Add(this.tableLayoutPanel22);
            this.tabCalDataPage.Location = new System.Drawing.Point(4, 22);
            this.tabCalDataPage.Name = "tabCalDataPage";
            this.tabCalDataPage.Padding = new System.Windows.Forms.Padding(3);
            this.tabCalDataPage.Size = new System.Drawing.Size(1181, 713);
            this.tabCalDataPage.TabIndex = 1;
            this.tabCalDataPage.Text = global::UPDConsole.Resources.TabCalDataPage;
            this.tabCalDataPage.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel22
            // 
            this.tableLayoutPanel22.ColumnCount = 2;
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 536F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel22.Controls.Add(this.tableLayoutPanel5, 0, 0);
            this.tableLayoutPanel22.Controls.Add(this.zg1, 1, 0);
            this.tableLayoutPanel22.Controls.Add(this.progressBarGraph, 0, 1);
            this.tableLayoutPanel22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel22.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel22.Name = "tableLayoutPanel22";
            this.tableLayoutPanel22.RowCount = 2;
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel22.Size = new System.Drawing.Size(1175, 707);
            this.tableLayoutPanel22.TabIndex = 0;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 53.1677F));
            this.tableLayoutPanel5.Controls.Add(this.gbxADCName, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.gbxGPSName, 0, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 81.57895F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 18.42105F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(530, 681);
            this.tableLayoutPanel5.TabIndex = 2;
            // 
            // gbxADCName
            // 
            this.gbxADCName.Controls.Add(this.tableLayoutPanel3);
            this.gbxADCName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbxADCName.Location = new System.Drawing.Point(3, 3);
            this.gbxADCName.Name = "gbxADCName";
            this.gbxADCName.Size = new System.Drawing.Size(524, 549);
            this.gbxADCName.TabIndex = 5;
            this.gbxADCName.TabStop = false;
            this.gbxADCName.Text = "Данные АЦП";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 7;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 59F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 63F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 58F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 134F));
            this.tableLayoutPanel3.Controls.Add(this.lblFrequencyADCName, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.txbFrequencyADC, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.rbtnFrequencyHorizontal, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.grbADCData, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.lblPeriodADCName, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.txbPeriodADC, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.btnStartStopDraw, 6, 1);
            this.tableLayoutPanel3.Controls.Add(this.txbTimeADC, 5, 1);
            this.tableLayoutPanel3.Controls.Add(this.lblTimeADCName, 4, 1);
            this.tableLayoutPanel3.Controls.Add(this.lblChannellADCName, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.btnFrequencyADCAllChannel, 6, 0);
            this.tableLayoutPanel3.Controls.Add(this.rbtnFrequencyVertical, 4, 0);
            this.tableLayoutPanel3.Controls.Add(this.txbChannellADC, 3, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(518, 530);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // lblFrequencyADCName
            // 
            this.lblFrequencyADCName.AutoSize = true;
            this.lblFrequencyADCName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblFrequencyADCName.Location = new System.Drawing.Point(3, 0);
            this.lblFrequencyADCName.Name = "lblFrequencyADCName";
            this.lblFrequencyADCName.Size = new System.Drawing.Size(53, 27);
            this.lblFrequencyADCName.TabIndex = 0;
            this.lblFrequencyADCName.Text = "Частота";
            this.lblFrequencyADCName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txbFrequencyADC
            // 
            this.txbFrequencyADC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txbFrequencyADC.Location = new System.Drawing.Point(62, 3);
            this.txbFrequencyADC.Maximum = new decimal(new int[] {
            64,
            0,
            0,
            0});
            this.txbFrequencyADC.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txbFrequencyADC.Name = "txbFrequencyADC";
            this.txbFrequencyADC.Size = new System.Drawing.Size(38, 20);
            this.txbFrequencyADC.TabIndex = 1;
            this.txbFrequencyADC.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // rbtnFrequencyHorizontal
            // 
            this.rbtnFrequencyHorizontal.AutoSize = true;
            this.rbtnFrequencyHorizontal.Checked = true;
            this.tableLayoutPanel3.SetColumnSpan(this.rbtnFrequencyHorizontal, 2);
            this.rbtnFrequencyHorizontal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtnFrequencyHorizontal.Location = new System.Drawing.Point(106, 3);
            this.rbtnFrequencyHorizontal.Name = "rbtnFrequencyHorizontal";
            this.rbtnFrequencyHorizontal.Size = new System.Drawing.Size(107, 21);
            this.rbtnFrequencyHorizontal.TabIndex = 4;
            this.rbtnFrequencyHorizontal.TabStop = true;
            this.rbtnFrequencyHorizontal.Text = "Горизонтальная";
            this.rbtnFrequencyHorizontal.UseVisualStyleBackColor = true;
            // 
            // grbADCData
            // 
            this.tableLayoutPanel3.SetColumnSpan(this.grbADCData, 7);
            this.grbADCData.Controls.Add(this.tblADCData);
            this.grbADCData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbADCData.Location = new System.Drawing.Point(3, 57);
            this.grbADCData.Name = "grbADCData";
            this.grbADCData.Size = new System.Drawing.Size(512, 470);
            this.grbADCData.TabIndex = 3;
            this.grbADCData.TabStop = false;
            // 
            // tblADCData
            // 
            this.tblADCData.ColumnCount = 4;
            this.tblADCData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tblADCData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tblADCData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tblADCData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tblADCData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblADCData.Location = new System.Drawing.Point(3, 16);
            this.tblADCData.Name = "tblADCData";
            this.tblADCData.RowCount = 1;
            this.tblADCData.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblADCData.Size = new System.Drawing.Size(506, 451);
            this.tblADCData.TabIndex = 0;
            // 
            // lblPeriodADCName
            // 
            this.lblPeriodADCName.AutoSize = true;
            this.lblPeriodADCName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPeriodADCName.Location = new System.Drawing.Point(3, 27);
            this.lblPeriodADCName.Name = "lblPeriodADCName";
            this.lblPeriodADCName.Size = new System.Drawing.Size(53, 27);
            this.lblPeriodADCName.TabIndex = 6;
            this.lblPeriodADCName.Text = "Отсчет";
            this.lblPeriodADCName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txbPeriodADC
            // 
            this.txbPeriodADC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txbPeriodADC.Location = new System.Drawing.Point(62, 30);
            this.txbPeriodADC.Maximum = new decimal(new int[] {
            32,
            0,
            0,
            0});
            this.txbPeriodADC.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txbPeriodADC.Name = "txbPeriodADC";
            this.txbPeriodADC.Size = new System.Drawing.Size(38, 20);
            this.txbPeriodADC.TabIndex = 7;
            this.txbPeriodADC.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // btnStartStopDraw
            // 
            this.btnStartStopDraw.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnStartStopDraw.Location = new System.Drawing.Point(387, 30);
            this.btnStartStopDraw.Name = "btnStartStopDraw";
            this.btnStartStopDraw.Size = new System.Drawing.Size(128, 21);
            this.btnStartStopDraw.TabIndex = 10;
            this.btnStartStopDraw.Text = "Построить график";
            this.btnStartStopDraw.UseVisualStyleBackColor = true;
            this.btnStartStopDraw.Click += new System.EventHandler(this.btnStartStopDraw_Click);
            // 
            // txbTimeADC
            // 
            this.txbTimeADC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txbTimeADC.Location = new System.Drawing.Point(329, 30);
            this.txbTimeADC.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.txbTimeADC.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txbTimeADC.Name = "txbTimeADC";
            this.txbTimeADC.Size = new System.Drawing.Size(52, 20);
            this.txbTimeADC.TabIndex = 9;
            this.txbTimeADC.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblTimeADCName
            // 
            this.lblTimeADCName.AutoSize = true;
            this.lblTimeADCName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTimeADCName.Location = new System.Drawing.Point(219, 27);
            this.lblTimeADCName.Name = "lblTimeADCName";
            this.lblTimeADCName.Size = new System.Drawing.Size(104, 27);
            this.lblTimeADCName.TabIndex = 8;
            this.lblTimeADCName.Text = "Время сбора, сек";
            this.lblTimeADCName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblChannellADCName
            // 
            this.lblChannellADCName.AutoSize = true;
            this.lblChannellADCName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblChannellADCName.Location = new System.Drawing.Point(106, 27);
            this.lblChannellADCName.Name = "lblChannellADCName";
            this.lblChannellADCName.Size = new System.Drawing.Size(57, 27);
            this.lblChannellADCName.TabIndex = 11;
            this.lblChannellADCName.Text = "Канал";
            this.lblChannellADCName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnFrequencyADCAllChannel
            // 
            this.btnFrequencyADCAllChannel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnFrequencyADCAllChannel.Location = new System.Drawing.Point(387, 3);
            this.btnFrequencyADCAllChannel.Name = "btnFrequencyADCAllChannel";
            this.btnFrequencyADCAllChannel.Size = new System.Drawing.Size(128, 21);
            this.btnFrequencyADCAllChannel.TabIndex = 2;
            this.btnFrequencyADCAllChannel.Text = "Показать все";
            this.btnFrequencyADCAllChannel.UseVisualStyleBackColor = true;
            this.btnFrequencyADCAllChannel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnPeriodADC_MouseDown);
            this.btnFrequencyADCAllChannel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnPeriodADC_MouseUp);
            // 
            // rbtnFrequencyVertical
            // 
            this.rbtnFrequencyVertical.AutoSize = true;
            this.tableLayoutPanel3.SetColumnSpan(this.rbtnFrequencyVertical, 2);
            this.rbtnFrequencyVertical.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtnFrequencyVertical.Location = new System.Drawing.Point(219, 3);
            this.rbtnFrequencyVertical.Name = "rbtnFrequencyVertical";
            this.rbtnFrequencyVertical.Size = new System.Drawing.Size(162, 21);
            this.rbtnFrequencyVertical.TabIndex = 5;
            this.rbtnFrequencyVertical.Text = "Вертикальная";
            this.rbtnFrequencyVertical.UseVisualStyleBackColor = true;
            // 
            // txbChannellADC
            // 
            this.txbChannellADC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txbChannellADC.Location = new System.Drawing.Point(169, 30);
            this.txbChannellADC.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.txbChannellADC.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txbChannellADC.Name = "txbChannellADC";
            this.txbChannellADC.Size = new System.Drawing.Size(44, 20);
            this.txbChannellADC.TabIndex = 12;
            this.txbChannellADC.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // gbxGPSName
            // 
            this.gbxGPSName.Controls.Add(this.tableLayoutPanel6);
            this.gbxGPSName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbxGPSName.Location = new System.Drawing.Point(3, 558);
            this.gbxGPSName.Name = "gbxGPSName";
            this.gbxGPSName.Size = new System.Drawing.Size(524, 120);
            this.gbxGPSName.TabIndex = 6;
            this.gbxGPSName.TabStop = false;
            this.gbxGPSName.Text = "Данные GPS";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 6;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel6.Controls.Add(this.lblGPSAltitude, 5, 1);
            this.tableLayoutPanel6.Controls.Add(this.lblGPSAltitudeName, 4, 1);
            this.tableLayoutPanel6.Controls.Add(this.lblGPSLongtitudeName, 2, 1);
            this.tableLayoutPanel6.Controls.Add(this.lblGPSLongtitude, 3, 1);
            this.tableLayoutPanel6.Controls.Add(this.lblGPSLatitudeName, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.lblGPSTimeName, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.lblGPSLatitude, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.lblGPSTime, 1, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 2;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(518, 101);
            this.tableLayoutPanel6.TabIndex = 4;
            // 
            // lblGPSAltitude
            // 
            this.lblGPSAltitude.AutoSize = true;
            this.lblGPSAltitude.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblGPSAltitude.Location = new System.Drawing.Point(433, 50);
            this.lblGPSAltitude.Name = "lblGPSAltitude";
            this.lblGPSAltitude.Size = new System.Drawing.Size(24, 51);
            this.lblGPSAltitude.TabIndex = 1;
            this.lblGPSAltitude.Text = "n/a";
            this.lblGPSAltitude.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGPSAltitudeName
            // 
            this.lblGPSAltitudeName.AutoSize = true;
            this.lblGPSAltitudeName.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblGPSAltitudeName.Location = new System.Drawing.Point(379, 50);
            this.lblGPSAltitudeName.Name = "lblGPSAltitudeName";
            this.lblGPSAltitudeName.Size = new System.Drawing.Size(48, 51);
            this.lblGPSAltitudeName.TabIndex = 0;
            this.lblGPSAltitudeName.Text = "Высота:";
            this.lblGPSAltitudeName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGPSLongtitudeName
            // 
            this.lblGPSLongtitudeName.AutoSize = true;
            this.lblGPSLongtitudeName.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblGPSLongtitudeName.Location = new System.Drawing.Point(202, 50);
            this.lblGPSLongtitudeName.Name = "lblGPSLongtitudeName";
            this.lblGPSLongtitudeName.Size = new System.Drawing.Size(53, 51);
            this.lblGPSLongtitudeName.TabIndex = 0;
            this.lblGPSLongtitudeName.Text = "Долгота:";
            this.lblGPSLongtitudeName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGPSLongtitude
            // 
            this.lblGPSLongtitude.AutoSize = true;
            this.lblGPSLongtitude.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblGPSLongtitude.Location = new System.Drawing.Point(261, 50);
            this.lblGPSLongtitude.Name = "lblGPSLongtitude";
            this.lblGPSLongtitude.Size = new System.Drawing.Size(24, 51);
            this.lblGPSLongtitude.TabIndex = 1;
            this.lblGPSLongtitude.Text = "n/a";
            this.lblGPSLongtitude.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGPSLatitudeName
            // 
            this.lblGPSLatitudeName.AutoSize = true;
            this.lblGPSLatitudeName.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblGPSLatitudeName.Location = new System.Drawing.Point(35, 50);
            this.lblGPSLatitudeName.Name = "lblGPSLatitudeName";
            this.lblGPSLatitudeName.Size = new System.Drawing.Size(48, 51);
            this.lblGPSLatitudeName.TabIndex = 0;
            this.lblGPSLatitudeName.Text = "Широта:";
            this.lblGPSLatitudeName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGPSTimeName
            // 
            this.lblGPSTimeName.AutoSize = true;
            this.lblGPSTimeName.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblGPSTimeName.Location = new System.Drawing.Point(40, 0);
            this.lblGPSTimeName.Name = "lblGPSTimeName";
            this.lblGPSTimeName.Size = new System.Drawing.Size(43, 50);
            this.lblGPSTimeName.TabIndex = 0;
            this.lblGPSTimeName.Text = "Время:";
            this.lblGPSTimeName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGPSLatitude
            // 
            this.lblGPSLatitude.AutoSize = true;
            this.lblGPSLatitude.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblGPSLatitude.Location = new System.Drawing.Point(89, 50);
            this.lblGPSLatitude.Name = "lblGPSLatitude";
            this.lblGPSLatitude.Size = new System.Drawing.Size(24, 51);
            this.lblGPSLatitude.TabIndex = 1;
            this.lblGPSLatitude.Text = "n/a";
            this.lblGPSLatitude.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGPSTime
            // 
            this.lblGPSTime.AutoSize = true;
            this.tableLayoutPanel6.SetColumnSpan(this.lblGPSTime, 5);
            this.lblGPSTime.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblGPSTime.Location = new System.Drawing.Point(89, 0);
            this.lblGPSTime.Name = "lblGPSTime";
            this.lblGPSTime.Size = new System.Drawing.Size(24, 50);
            this.lblGPSTime.TabIndex = 1;
            this.lblGPSTime.Text = "n/a";
            this.lblGPSTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // zg1
            // 
            this.zg1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zg1.Location = new System.Drawing.Point(539, 3);
            this.zg1.Name = "zg1";
            this.zg1.ScrollGrace = 0D;
            this.zg1.ScrollMaxX = 0D;
            this.zg1.ScrollMaxY = 0D;
            this.zg1.ScrollMaxY2 = 0D;
            this.zg1.ScrollMinX = 0D;
            this.zg1.ScrollMinY = 0D;
            this.zg1.ScrollMinY2 = 0D;
            this.zg1.Size = new System.Drawing.Size(633, 681);
            this.zg1.TabIndex = 3;
            // 
            // progressBarGraph
            // 
            this.tableLayoutPanel22.SetColumnSpan(this.progressBarGraph, 2);
            this.progressBarGraph.Dock = System.Windows.Forms.DockStyle.Fill;
            this.progressBarGraph.Location = new System.Drawing.Point(3, 690);
            this.progressBarGraph.Name = "progressBarGraph";
            this.progressBarGraph.Size = new System.Drawing.Size(1169, 14);
            this.progressBarGraph.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1189, 763);
            this.Controls.Add(this.tabData);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(1205, 758);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.gbxRawADCName.ResumeLayout(false);
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel14.PerformLayout();
            this.tableLayoutPanel15.ResumeLayout(false);
            this.tableLayoutPanel15.PerformLayout();
            this.gbxRawGPSName.ResumeLayout(false);
            this.tableLayoutPanel16.ResumeLayout(false);
            this.tableLayoutPanel18.ResumeLayout(false);
            this.tableLayoutPanel18.PerformLayout();
            this.tableLayoutPanel17.ResumeLayout(false);
            this.tableLayoutPanel17.PerformLayout();
            this.gbxTransmiteName.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabData.ResumeLayout(false);
            this.tabRAWDataPage.ResumeLayout(false);
            this.tableLayoutPanel21.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.grbxMessage.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numDelay)).EndInit();
            this.pnlMessageConfigRegister.ResumeLayout(false);
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numBitsFrq)).EndInit();
            this.pnlMessageControlFSMC.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.pnlMessageControlGPS.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel12.PerformLayout();
            this.grbxMessageList.ResumeLayout(false);
            this.tblMessageList.ResumeLayout(false);
            this.tblMessageList.PerformLayout();
            this.tabCalDataPage.ResumeLayout(false);
            this.tableLayoutPanel22.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.gbxADCName.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txbFrequencyADC)).EndInit();
            this.grbADCData.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txbPeriodADC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbTimeADC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbChannellADC)).EndInit();
            this.gbxGPSName.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.RichTextBox txbTransmiteData;
        private System.Windows.Forms.Button btnTransmiteData;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuConnect;
        private System.Windows.Forms.ToolStripMenuItem menuDisconnect;
        private System.Windows.Forms.GroupBox gbxRawADCName;
        private System.Windows.Forms.GroupBox gbxRawGPSName;
        private System.Windows.Forms.GroupBox gbxTransmiteName;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TabControl tabData;
        private System.Windows.Forms.TabPage tabRAWDataPage;
        private System.Windows.Forms.TabPage tabCalDataPage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.GroupBox gbxADCName;
        private System.Windows.Forms.GroupBox gbxGPSName;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label lblGPSTimeName;
        private System.Windows.Forms.Label lblGPSLatitudeName;
        private System.Windows.Forms.Label lblGPSLongtitudeName;
        private System.Windows.Forms.Label lblGPSAltitudeName;
        private System.Windows.Forms.Label lblGPSTime;
        private System.Windows.Forms.Label lblGPSLatitude;
        private System.Windows.Forms.Label lblGPSLongtitude;
        private System.Windows.Forms.Label lblGPSAltitude;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label lblFrequencyADCName;
        private System.Windows.Forms.NumericUpDown txbFrequencyADC;
        private System.Windows.Forms.Button btnFrequencyADCAllChannel;
        private System.Windows.Forms.GroupBox grbADCData;
        private System.Windows.Forms.TableLayoutPanel tblADCData;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.GroupBox grbxMessage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.RadioButton rbtnEmpty;
        private System.Windows.Forms.RadioButton rbtnConfigRegister;
        private System.Windows.Forms.RadioButton rbtnControlFSMC;
        private System.Windows.Forms.RadioButton rbtnControlGPS;
        private System.Windows.Forms.Panel pnlMessageConfigRegister;
        private System.Windows.Forms.Panel pnlMessageControlFSMC;
        private System.Windows.Forms.Panel pnlMessageControlGPS;
        private System.Windows.Forms.Panel pnlMessageEmpty;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.RadioButton rbtnFSMCOn;
        private System.Windows.Forms.RadioButton rbtnFSMCOff;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.RadioButton rbtnGPSOn;
        private System.Windows.Forms.RadioButton rbtnGPSOff;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.CheckBox chbxBitTest;
        private System.Windows.Forms.CheckBox chbxBitTestMode;
        private System.Windows.Forms.CheckBox chbxBitTxOn;
        private System.Windows.Forms.CheckBox chbxBitEnACQ;
        private System.Windows.Forms.Label lblFrequncyName;
        private System.Windows.Forms.NumericUpDown numBitsFrq;
        private System.Windows.Forms.CheckBox chbxBitManual;
        private System.Windows.Forms.Button btnApplyMessage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.Button btnTaskClear;
        private System.Windows.Forms.Button btnTaskStart;
        private System.Windows.Forms.Button btnTaskStop;
        private System.Windows.Forms.CheckBox chbxCircylarSending;
        private System.Windows.Forms.GroupBox grbxMessageList;
        private System.Windows.Forms.TableLayoutPanel tblMessageList;
        private System.Windows.Forms.Button btnSendMessage1;
        private System.Windows.Forms.Button btnSendMessage2;
        private System.Windows.Forms.Button btnSendMessage3;
        private System.Windows.Forms.Button btnSendMessage4;
        private System.Windows.Forms.Button btnSendMessage5;
        private System.Windows.Forms.Button btnSendMessage6;
        private System.Windows.Forms.Button btnSendMessage7;
        private System.Windows.Forms.Button btnSendMessage8;
        private System.Windows.Forms.Button btnSendMessage9;
        private System.Windows.Forms.Button btnSendMessage10;
        private System.Windows.Forms.Label lblDelay10;
        private System.Windows.Forms.Label lblDelay9;
        private System.Windows.Forms.Label lblDelay8;
        private System.Windows.Forms.Label lblDelay7;
        private System.Windows.Forms.Label lblDelay6;
        private System.Windows.Forms.Label lblDelay5;
        private System.Windows.Forms.Label lblDelay4;
        private System.Windows.Forms.Label lblDelay3;
        private System.Windows.Forms.Label lblDelay2;
        private System.Windows.Forms.Label lblDelay1;
        private System.Windows.Forms.Label lblMessage1;
        private System.Windows.Forms.Label lblMessage2;
        private System.Windows.Forms.Label lblMessage3;
        private System.Windows.Forms.Label lblMessage4;
        private System.Windows.Forms.Label lblMessage5;
        private System.Windows.Forms.Label lblMessage6;
        private System.Windows.Forms.Label lblMessage7;
        private System.Windows.Forms.Label lblMessage8;
        private System.Windows.Forms.Label lblMessage9;
        private System.Windows.Forms.Label lblMessage10;
        private System.Windows.Forms.RadioButton rbtnMessage1;
        private System.Windows.Forms.RadioButton rbtnMessage2;
        private System.Windows.Forms.RadioButton rbtnMessage3;
        private System.Windows.Forms.RadioButton rbtnMessage4;
        private System.Windows.Forms.RadioButton rbtnMessage5;
        private System.Windows.Forms.RadioButton rbtnMessage6;
        private System.Windows.Forms.RadioButton rbtnMessage7;
        private System.Windows.Forms.RadioButton rbtnMessage8;
        private System.Windows.Forms.RadioButton rbtnMessage9;
        private System.Windows.Forms.RadioButton rbtnMessage10;
        private System.Windows.Forms.NumericUpDown numDelay;
        private System.Windows.Forms.Label lblDelay;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.RichTextBox txbReceiveADCData;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.CheckBox chbxRawADCVeiwNumber;
        private System.Windows.Forms.CheckBox chbxRawADCVeiwTime;
        private System.Windows.Forms.CheckBox chbxRawADCVeiwData;
        private System.Windows.Forms.Button btnRawADCCleare;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private System.Windows.Forms.Label lblRawADCCounterTotal;
        private System.Windows.Forms.Label lblRawADCCounterError;
        private System.Windows.Forms.Button btnRawADCCounterReset;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel18;
        private System.Windows.Forms.Label lblRawGPSCounterTotal;
        private System.Windows.Forms.Label lblRawGPSCounterError;
        private System.Windows.Forms.Button btnRawGPSCounterReset;
        private System.Windows.Forms.RichTextBox txbReceiveGPSData;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel17;
        private System.Windows.Forms.CheckBox chbxRawGPSVeiwNumber;
        private System.Windows.Forms.CheckBox chbxRawGPSVeiwTime;
        private System.Windows.Forms.CheckBox chbxRawGPSVeiwData;
        private System.Windows.Forms.Button btnRawGPSClearn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel21;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel22;
        private System.Windows.Forms.RadioButton rbtnFrequencyHorizontal;
        private System.Windows.Forms.RadioButton rbtnFrequencyVertical;
        private System.Windows.Forms.Label lblPeriodADCName;
        private System.Windows.Forms.NumericUpDown txbPeriodADC;
        private System.Windows.Forms.Button btnStartStopDraw;
        private System.Windows.Forms.NumericUpDown txbTimeADC;
        private System.Windows.Forms.Label lblTimeADCName;
        private System.Windows.Forms.Label lblChannellADCName;
        private System.Windows.Forms.NumericUpDown txbChannellADC;
        private ZedGraph.ZedGraphControl zg1;
        private System.Windows.Forms.ProgressBar progressBarGraph;

    }
}

