﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace UPDConsole
{
    public class SettingsConfigData
    {
        public string ComPort;
        public UInt32 BaudRate;
        public string ControllerIPAddress;
        public string ControllerIPMask;
        public UInt16 ControllerPort;
        public string PCIPAddress;
        public UInt16 PCPort;

        public SettingsConfigData()
        {
        }

        public SettingsConfigData(bool reading)
        {
            ComPort = Consts.CONFIG_INIT_PORT;
            BaudRate = Consts.CONFIG_INIT_BAUDRATE;
            ControllerIPAddress = Consts.CONFIG_INIT_CONTROLLER_IP;
            ControllerIPMask = Consts.CONFIG_INIT_CONTROLLER_MASK;
            ControllerPort = Consts.CONFIG_INIT_CONTROLLER_PORT;
            PCIPAddress = Consts.CONFIG_INIT_PC_IP;
            PCPort = Consts.CONFIG_INIT_PC_PORT;
            if (reading)
            {
                readFromFile();
            }
        }

        public void SetConfigData(string port, string baudrate, string ctrlIP, string ctrlMask,
            string ctrlPort, string pcIP, string pcPort)
        {
            ComPort = port;
            BaudRate = Convert.ToUInt32(baudrate);
            ControllerIPAddress = ctrlIP;
            ControllerIPMask = ctrlMask;
            ControllerPort = Convert.ToUInt16(ctrlPort);
            PCIPAddress = pcIP;
            PCPort = Convert.ToUInt16(pcPort);
        }

        public bool IsNull()
        {
            if (ComPort == null || BaudRate == null || ControllerIPAddress == null ||
                ControllerIPMask == null || ControllerPort == null ||
                PCIPAddress == null || PCPort == null)
            {
                return true;
            }
            return false;
        }

        public void readFromFile()
        {
            XmlSerializer x = new XmlSerializer(typeof(SettingsConfigData));
            TextReader reader = null;
            try
            {
                reader = new StreamReader(Consts.CONFIG_FILE_NAME);
            }
            catch (FileNotFoundException exc)
            {
                writeToFile();
                return;
            }
            SettingsConfigData settings = (SettingsConfigData)x.Deserialize(reader);
            if (!settings.IsNull())
            {
                SetConfigData(settings.ComPort, settings.BaudRate.ToString(), settings.ControllerIPAddress,
                    settings.ControllerIPMask, settings.ControllerPort.ToString(),
                    settings.PCIPAddress, settings.PCPort.ToString());
            }
            reader.Close();
        }

        public void writeToFile()
        {
            XmlSerializer x = new XmlSerializer(typeof(SettingsConfigData));
            TextWriter writer = new StreamWriter(Consts.CONFIG_FILE_NAME, false);
            x.Serialize(writer, this);
            writer.Close();
        }
    }
}
