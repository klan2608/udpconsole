﻿namespace UPDConsole
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbxComPort = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblComPortName = new System.Windows.Forms.Label();
            this.lblComBoudRateName = new System.Windows.Forms.Label();
            this.lblControllerIPAddressName = new System.Windows.Forms.Label();
            this.cbxBoudRate = new System.Windows.Forms.ComboBox();
            this.lblPCTransmitePortName = new System.Windows.Forms.Label();
            this.lblPCReceivePortName = new System.Windows.Forms.Label();
            this.lblPCIPAddressName = new System.Windows.Forms.Label();
            this.lblControllerTransmitePortName = new System.Windows.Forms.Label();
            this.lblControllerReceivePortName = new System.Windows.Forms.Label();
            this.lblControllerIPMaskName = new System.Windows.Forms.Label();
            this.lblState = new System.Windows.Forms.Label();
            this.btnWriteConfiguration = new System.Windows.Forms.Button();
            this.btnReadConfiguration = new System.Windows.Forms.Button();
            this.btnConnect = new System.Windows.Forms.Button();
            this.txbControllerIPAddress = new System.Windows.Forms.TextBox();
            this.txbControllerIPMask = new System.Windows.Forms.TextBox();
            this.txbControllerReceivePort = new System.Windows.Forms.TextBox();
            this.txbControllerTransmitePort = new System.Windows.Forms.TextBox();
            this.txbPCReceivePort = new System.Windows.Forms.TextBox();
            this.txbPCTransmitePort = new System.Windows.Forms.TextBox();
            this.txbPCIPAddress = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbxComPort
            // 
            this.cbxComPort.Dock = System.Windows.Forms.DockStyle.Left;
            this.cbxComPort.FormattingEnabled = true;
            this.cbxComPort.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cbxComPort.Items.AddRange(new object[] {
            "COM1",
            "COM2",
            "COM3",
            "COM4",
            "COM5",
            "COM6",
            "COM7",
            "COM8",
            "COM9",
            "COM10",
            "COM11",
            "COM12",
            "COM13",
            "COM14",
            "COM15",
            "COM16",
            "COM17",
            "COM18",
            "COM19",
            "COM20",
            "COM21",
            "COM22",
            "COM23",
            "COM24",
            "COM25",
            "COM26",
            "COM27",
            "COM28",
            "COM29",
            "COM30"});
            this.cbxComPort.Location = new System.Drawing.Point(187, 13);
            this.cbxComPort.Name = "cbxComPort";
            this.cbxComPort.Size = new System.Drawing.Size(63, 21);
            this.cbxComPort.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 38.27434F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.78761F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 39.15929F));
            this.tableLayoutPanel1.Controls.Add(this.cbxComPort, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblComPortName, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblComBoudRateName, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblControllerIPAddressName, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.cbxBoudRate, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblPCTransmitePortName, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.lblPCReceivePortName, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.lblPCIPAddressName, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.lblControllerTransmitePortName, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.lblControllerReceivePortName, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblControllerIPMaskName, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblState, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnWriteConfiguration, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.btnReadConfiguration, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.btnConnect, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.txbControllerIPAddress, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.txbControllerIPMask, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.txbControllerReceivePort, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.txbControllerTransmitePort, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.txbPCReceivePort, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.txbPCTransmitePort, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.txbPCIPAddress, 1, 6);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(15);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.RowCount = 9;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.49953F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.49953F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.49953F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.49953F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.49953F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.49953F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.49953F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.50328F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(477, 245);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // lblComPortName
            // 
            this.lblComPortName.AutoSize = true;
            this.lblComPortName.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblComPortName.Location = new System.Drawing.Point(13, 10);
            this.lblComPortName.Name = "lblComPortName";
            this.lblComPortName.Size = new System.Drawing.Size(85, 25);
            this.lblComPortName.TabIndex = 0;
            this.lblComPortName.Text = "lblComPortName";
            this.lblComPortName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblComBoudRateName
            // 
            this.lblComBoudRateName.AutoSize = true;
            this.lblComBoudRateName.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblComBoudRateName.Location = new System.Drawing.Point(13, 35);
            this.lblComBoudRateName.Name = "lblComBoudRateName";
            this.lblComBoudRateName.Size = new System.Drawing.Size(114, 25);
            this.lblComBoudRateName.TabIndex = 0;
            this.lblComBoudRateName.Text = "lblComBoudRateName";
            this.lblComBoudRateName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblControllerIPAddressName
            // 
            this.lblControllerIPAddressName.AutoSize = true;
            this.lblControllerIPAddressName.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblControllerIPAddressName.Location = new System.Drawing.Point(13, 60);
            this.lblControllerIPAddressName.Name = "lblControllerIPAddressName";
            this.lblControllerIPAddressName.Size = new System.Drawing.Size(137, 25);
            this.lblControllerIPAddressName.TabIndex = 0;
            this.lblControllerIPAddressName.Text = "lblControllerIPAddressName";
            this.lblControllerIPAddressName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbxBoudRate
            // 
            this.cbxBoudRate.FormattingEnabled = true;
            this.cbxBoudRate.Items.AddRange(new object[] {
            "9600",
            "115200"});
            this.cbxBoudRate.Location = new System.Drawing.Point(187, 38);
            this.cbxBoudRate.Name = "cbxBoudRate";
            this.cbxBoudRate.Size = new System.Drawing.Size(63, 21);
            this.cbxBoudRate.TabIndex = 2;
            // 
            // lblPCTransmitePortName
            // 
            this.lblPCTransmitePortName.AutoSize = true;
            this.lblPCTransmitePortName.Location = new System.Drawing.Point(13, 210);
            this.lblPCTransmitePortName.Name = "lblPCTransmitePortName";
            this.lblPCTransmitePortName.Size = new System.Drawing.Size(124, 13);
            this.lblPCTransmitePortName.TabIndex = 0;
            this.lblPCTransmitePortName.Text = "lblPCTransmitePortName";
            this.lblPCTransmitePortName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPCReceivePortName
            // 
            this.lblPCReceivePortName.AutoSize = true;
            this.lblPCReceivePortName.Location = new System.Drawing.Point(13, 185);
            this.lblPCReceivePortName.Name = "lblPCReceivePortName";
            this.lblPCReceivePortName.Size = new System.Drawing.Size(118, 13);
            this.lblPCReceivePortName.TabIndex = 0;
            this.lblPCReceivePortName.Text = "lblPCReceivePortName";
            this.lblPCReceivePortName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPCIPAddressName
            // 
            this.lblPCIPAddressName.AutoSize = true;
            this.lblPCIPAddressName.Location = new System.Drawing.Point(13, 160);
            this.lblPCIPAddressName.Name = "lblPCIPAddressName";
            this.lblPCIPAddressName.Size = new System.Drawing.Size(107, 13);
            this.lblPCIPAddressName.TabIndex = 0;
            this.lblPCIPAddressName.Text = "lblPCIPAddressName";
            this.lblPCIPAddressName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblControllerTransmitePortName
            // 
            this.lblControllerTransmitePortName.AutoSize = true;
            this.lblControllerTransmitePortName.Location = new System.Drawing.Point(13, 135);
            this.lblControllerTransmitePortName.Name = "lblControllerTransmitePortName";
            this.lblControllerTransmitePortName.Size = new System.Drawing.Size(154, 13);
            this.lblControllerTransmitePortName.TabIndex = 0;
            this.lblControllerTransmitePortName.Text = "lblControllerTransmitePortName";
            this.lblControllerTransmitePortName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblControllerReceivePortName
            // 
            this.lblControllerReceivePortName.AutoSize = true;
            this.lblControllerReceivePortName.Location = new System.Drawing.Point(13, 110);
            this.lblControllerReceivePortName.Name = "lblControllerReceivePortName";
            this.lblControllerReceivePortName.Size = new System.Drawing.Size(148, 13);
            this.lblControllerReceivePortName.TabIndex = 0;
            this.lblControllerReceivePortName.Text = "lblControllerReceivePortName";
            this.lblControllerReceivePortName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblControllerIPMaskName
            // 
            this.lblControllerIPMaskName.AutoSize = true;
            this.lblControllerIPMaskName.Location = new System.Drawing.Point(13, 85);
            this.lblControllerIPMaskName.Name = "lblControllerIPMaskName";
            this.lblControllerIPMaskName.Size = new System.Drawing.Size(125, 13);
            this.lblControllerIPMaskName.TabIndex = 0;
            this.lblControllerIPMaskName.Text = "lblControllerIPMaskName";
            // 
            // lblState
            // 
            this.lblState.AutoSize = true;
            this.lblState.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblState.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblState.ForeColor = System.Drawing.Color.Red;
            this.lblState.Location = new System.Drawing.Point(290, 10);
            this.lblState.Name = "lblState";
            this.tableLayoutPanel1.SetRowSpan(this.lblState, 3);
            this.lblState.Size = new System.Drawing.Size(174, 75);
            this.lblState.TabIndex = 16;
            this.lblState.Text = "lblState";
            this.lblState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnWriteConfiguration
            // 
            this.btnWriteConfiguration.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnWriteConfiguration.Location = new System.Drawing.Point(290, 188);
            this.btnWriteConfiguration.Name = "btnWriteConfiguration";
            this.tableLayoutPanel1.SetRowSpan(this.btnWriteConfiguration, 2);
            this.btnWriteConfiguration.Size = new System.Drawing.Size(174, 44);
            this.btnWriteConfiguration.TabIndex = 12;
            this.btnWriteConfiguration.Text = "btnWriteConfiguration";
            this.btnWriteConfiguration.UseVisualStyleBackColor = true;
            this.btnWriteConfiguration.Click += new System.EventHandler(this.btnWriteConfiguration_Click);
            // 
            // btnReadConfiguration
            // 
            this.btnReadConfiguration.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnReadConfiguration.Location = new System.Drawing.Point(290, 138);
            this.btnReadConfiguration.Name = "btnReadConfiguration";
            this.tableLayoutPanel1.SetRowSpan(this.btnReadConfiguration, 2);
            this.btnReadConfiguration.Size = new System.Drawing.Size(174, 44);
            this.btnReadConfiguration.TabIndex = 4;
            this.btnReadConfiguration.Text = "btnReadConfiguration";
            this.btnReadConfiguration.UseVisualStyleBackColor = true;
            this.btnReadConfiguration.Click += new System.EventHandler(this.btnReadConfiguration_Click);
            // 
            // btnConnect
            // 
            this.btnConnect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnConnect.Location = new System.Drawing.Point(290, 88);
            this.btnConnect.Name = "btnConnect";
            this.tableLayoutPanel1.SetRowSpan(this.btnConnect, 2);
            this.btnConnect.Size = new System.Drawing.Size(174, 44);
            this.btnConnect.TabIndex = 3;
            this.btnConnect.Text = "btnConnect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // txbControllerIPAddress
            // 
            this.txbControllerIPAddress.Location = new System.Drawing.Point(187, 63);
            this.txbControllerIPAddress.Name = "txbControllerIPAddress";
            this.txbControllerIPAddress.Size = new System.Drawing.Size(97, 20);
            this.txbControllerIPAddress.TabIndex = 17;
            // 
            // txbControllerIPMask
            // 
            this.txbControllerIPMask.Location = new System.Drawing.Point(187, 88);
            this.txbControllerIPMask.Name = "txbControllerIPMask";
            this.txbControllerIPMask.Size = new System.Drawing.Size(97, 20);
            this.txbControllerIPMask.TabIndex = 18;
            // 
            // txbControllerReceivePort
            // 
            this.txbControllerReceivePort.Location = new System.Drawing.Point(187, 113);
            this.txbControllerReceivePort.Name = "txbControllerReceivePort";
            this.txbControllerReceivePort.Size = new System.Drawing.Size(63, 20);
            this.txbControllerReceivePort.TabIndex = 19;
            // 
            // txbControllerTransmitePort
            // 
            this.txbControllerTransmitePort.Location = new System.Drawing.Point(187, 138);
            this.txbControllerTransmitePort.Name = "txbControllerTransmitePort";
            this.txbControllerTransmitePort.Size = new System.Drawing.Size(63, 20);
            this.txbControllerTransmitePort.TabIndex = 20;
            // 
            // txbPCReceivePort
            // 
            this.txbPCReceivePort.Location = new System.Drawing.Point(187, 188);
            this.txbPCReceivePort.Name = "txbPCReceivePort";
            this.txbPCReceivePort.Size = new System.Drawing.Size(63, 20);
            this.txbPCReceivePort.TabIndex = 21;
            // 
            // txbPCTransmitePort
            // 
            this.txbPCTransmitePort.Location = new System.Drawing.Point(187, 213);
            this.txbPCTransmitePort.Name = "txbPCTransmitePort";
            this.txbPCTransmitePort.Size = new System.Drawing.Size(63, 20);
            this.txbPCTransmitePort.TabIndex = 22;
            // 
            // txbPCIPAddress
            // 
            this.txbPCIPAddress.Location = new System.Drawing.Point(187, 163);
            this.txbPCIPAddress.Name = "txbPCIPAddress";
            this.txbPCIPAddress.Size = new System.Drawing.Size(97, 20);
            this.txbPCIPAddress.TabIndex = 17;
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(477, 245);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Settings";
            this.Text = "Settings";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Settings_FormClosing);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbxComPort;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblComPortName;
        private System.Windows.Forms.Label lblComBoudRateName;
        private System.Windows.Forms.Label lblControllerIPAddressName;
        private System.Windows.Forms.Label lblControllerReceivePortName;
        private System.Windows.Forms.Label lblControllerTransmitePortName;
        private System.Windows.Forms.Label lblPCIPAddressName;
        private System.Windows.Forms.Label lblPCReceivePortName;
        private System.Windows.Forms.Label lblPCTransmitePortName;
        private System.Windows.Forms.ComboBox cbxBoudRate;
        private System.Windows.Forms.Label lblControllerIPMaskName;
        private System.Windows.Forms.Label lblState;
        private System.Windows.Forms.Button btnWriteConfiguration;
        private System.Windows.Forms.Button btnReadConfiguration;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.TextBox txbControllerIPAddress;
        private System.Windows.Forms.TextBox txbControllerIPMask;
        private System.Windows.Forms.TextBox txbControllerReceivePort;
        private System.Windows.Forms.TextBox txbControllerTransmitePort;
        private System.Windows.Forms.TextBox txbPCReceivePort;
        private System.Windows.Forms.TextBox txbPCTransmitePort;
        private System.Windows.Forms.TextBox txbPCIPAddress;
    }
}