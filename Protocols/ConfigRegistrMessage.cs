﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UPDConsole.Protocols
{
    internal class ConfigRegistrMessage : MyMessage
    {
        internal bool _test;
        internal bool _testMode;
        internal bool _txOn;
        internal bool _enACQ;
        internal byte _frq;
        internal bool _man;
        internal ConfigRegistrMessage(bool test, bool testMode, bool txOn, bool enACQ, byte frq, bool man, int delay)
        {
            _delay = delay;
            _test = test;
            _testMode = testMode;
            _txOn = txOn;
            _enACQ = enACQ;
            _frq = frq;
            _man = man;

            _message = new byte[8];
            _message[0] = (byte)(Consts.PACKAGE_HEAD_SETTINGS >> 8);
            _message[1] = (byte)(Consts.PACKAGE_HEAD_SETTINGS & 0xFF);
            _message[2] = 0;
            _message[3] = 0;
            _message[4] = (byte)(Consts.PACKAGE_SETTINGS_TYPE_CONFIG_REGISTER >> 8);
            _message[5] = (byte)(Consts.PACKAGE_SETTINGS_TYPE_CONFIG_REGISTER & 0xFF);
            UInt16 value = 0;
            value |= (UInt16)(test ? 0x0001 : 0x0000);
            value |= (UInt16)(testMode ? 0x0002 : 0x0000);
            value |= (UInt16)(txOn ? 0x0010 : 0x0000);
            value |= (UInt16)(enACQ ? 0x0020 : 0x0000);
            value |= (UInt16)((frq & 0x3F) << 8);
            value |= (UInt16)(man ? 0x4000 : 0x0000);

            _message[6] = (byte)(value >> 8);
            _message[7] = (byte)(value & 0xFF);
        }
    }
}
