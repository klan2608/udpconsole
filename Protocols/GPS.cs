﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace UPDConsole.Protocols
{
    internal class GPS : Response
    {
        internal string Time { set; get; }
        internal string Latitude { set; get; }
        internal string Longtitude { set; get; }
        internal string Altitude { set; get; }
        
        public GPS(byte[] message)
        {
            string messageStr = Encoding.ASCII.GetString(message);

            _messageNumber = (int)Response.GetValue(message, 2, 3, 4, 5);
            //_time = Response.GetValue(message, 4, 5);
            _time = Response.GetValue(message, 6, 7, 8, 9);
            _message = new byte[message.Length - 10];
//            Array.Copy(message, 6, _message, 0, message.Length - 6);
            Array.Copy(message, 10, _message, 0, message.Length - 10);

            //$GNGGA,210133.00,5356.02041,N,02741.53914,E,1,10,1.10,246.0,M,24.8,M,,*47
            int GGAIndex = messageStr.IndexOf("$GNGGA");
            int timeStartIndex = messageStr.IndexOf(",",GGAIndex) + 1;
            int timeStopIndex = messageStr.IndexOf(",", timeStartIndex);
            int latitudeStartIndex = timeStopIndex + 1;
            int latitudeStopIndex = messageStr.IndexOf(",", messageStr.IndexOf(",", latitudeStartIndex) + 1);
            int lontitudeStartIndex = latitudeStopIndex + 1;
            int lontitudeStopIndex = messageStr.IndexOf(",", messageStr.IndexOf(",", lontitudeStartIndex) + 1);
            int altitudeStartIndex = messageStr.IndexOf(",", messageStr.IndexOf(",", messageStr.IndexOf(",", lontitudeStopIndex + 1) + 1) + 1) + 1;
            int altitudeStopIndex = messageStr.IndexOf(",", messageStr.IndexOf(",", altitudeStartIndex) + 1);


            //int GGAIndex = messageStr.IndexOf("$GNGGA");
            if (GGAIndex >= 0 && GGAIndex + 54 < messageStr.Length)
            {
                //Time = messageStr.Substring(GGAIndex + 7, 10).Insert(2, ":").Insert(5, ":");
                //Latitude = messageStr.Substring(GGAIndex + 17 + 1, 11).Replace(',', ' ');
                //Longtitude = messageStr.Substring(GGAIndex + 18 + 12, 12).Replace(',', ' ');

                //int AltitudeStartIndex = GGAIndex + 30 + 22;
                //int AltitudeSize = messageStr.IndexOf(',', AltitudeStartIndex) - AltitudeStartIndex;
                //if (AltitudeSize > 0)
                //{
                //    Altitude = messageStr.Substring(AltitudeStartIndex, AltitudeSize + 2).Replace(',', ' ');
                //}
                Time = messageStr.Substring(timeStartIndex, timeStopIndex - timeStartIndex ).Insert(2, ":").Insert(5, ":");
                Latitude = messageStr.Substring(latitudeStartIndex, latitudeStopIndex - latitudeStartIndex).Replace(',', ' ');
                Longtitude = messageStr.Substring(lontitudeStartIndex, lontitudeStopIndex - lontitudeStartIndex).Replace(',', ' ');
                Altitude = messageStr.Substring(altitudeStartIndex, altitudeStopIndex - altitudeStartIndex).Replace(',', ' ');
            }
            else
            {
                Time = "n/a";
                Latitude = "n/a";
                Longtitude = "n/a";
                Altitude = "n/a";
            }
        }

        //$GPRMC,161229.487,A,3723.2475,N,12158.3416,W,0.13,309.62,120598,,*10
        //$GPGGA, 161229.487, 3723.2475,N, 12158.3416,W,1,07,1.0, 9.0,M,,,,0000*18
        //public static GPS Parse(string message)
        //{
        //    int GGAIndex = message.IndexOf("$GPGGA");
        //    if (GGAIndex >= 0)
        //    {
        //        newData.Time = message.Substring(GGAIndex + 7, 10);
        //        newData.Time = newData.Time.Insert(2, ":").Insert(5, ":");
        //        newData.Latitude = message.Substring(GGAIndex + 17 + 1, 11);
        //        newData.Latitude = newData.Latitude.Replace(',', ' ');
        //        newData.Longtitude = message.Substring(GGAIndex + 18 + 12, 12);
        //        newData.Longtitude = newData.Longtitude.Replace(',', ' ');

        //        int AltitudeStartIndex = GGAIndex + 30 + 22;
        //        int AltitudeSize = message.IndexOf(',', AltitudeStartIndex) - AltitudeStartIndex;
        //        if (AltitudeSize > 0)
        //        {
        //            newData.Altitude = message.Substring(AltitudeStartIndex, AltitudeSize + 2);
        //            newData.Altitude = newData.Altitude.Replace(',', ' ');
        //        }
        //    }
        //    return newData;
        //}

        internal override string Print(bool number, bool time, bool data)
        {
            string message = "\n<< ";

            if (number)
            {
                message += string.Format("Пакет № {0} ", _messageNumber);
            }

            if (time)
            {
                message += string.Format("Время {0}", _time);
            }

            if (data)
            {
                message += string.Format("\n{0}", Encoding.ASCII.GetString(_message));
            }

            return message;
        }
    }
}
