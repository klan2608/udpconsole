﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UPDConsole.Protocols
{
    internal abstract class Response
    {
        internal int _messageNumber;
        internal UInt32 _time;
        internal byte[] _message;

        internal static Response GetResponse(byte[] message)
        {
            if(message.Length > 2)
            {
                if(GetValue(message, 0, 1) == Consts.PACKAGE_HEAD_ADC)
                {
                    if(message.Length == Consts.PACKAGE_SIZE_ADC)
                    {
                        return new ADC(message);
                    }
                }
                else if(GetValue(message, 0, 1) == Consts.PACKAGE_HEAD_GPS)
                {
                    if(message.Length > Consts.PACKAGE_MIN_SIZE_GPS)
                    {
                        return new GPS(message);
                    }
                }
            }
            return null;
        }

        internal abstract string Print(bool number, bool time, bool data);

        internal static UInt16 GetValue(byte[] message, int indexH, int indexL)
        {
            if (indexH < message.Length && indexL < message.Length)
            {
                return (UInt16)((message[indexH] << 8) | message[indexL]);
            }
            return 0;
        }

        internal static UInt32 GetValue(byte[] message, int indexHH, int indexH, int indexL, int indexLL)
        {
            if (indexHH < message.Length && indexH < message.Length && indexL < message.Length && indexLL < message.Length)
            {
                return (UInt32)((message[indexHH] << 24) | (message[indexH] << 16) | (message[indexL] << 8) | message[indexLL]);
            }
            return 0;
        }
    }
}
