﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UPDConsole
{
    internal enum Polarization
    {
        NO = 0,
        HORIZONTAL,
        VERTICAL,
        BOTH
    }

    internal enum TypeMassage
    {
        EMPTY = 0,
        CONFIG_REGISTER,
        CONTROL_FSMC,
        CONTROL_GPS
    }

    internal class Consts
    {
        internal const UInt16 PACKAGE_HEAD_ADC = 0x3301;
        internal const UInt16 PACKAGE_HEAD_GPS = 0x3302;
        internal const UInt16 PACKAGE_HEAD_SETTINGS = 0x3304;

        internal const UInt16 PACKAGE_SETTINGS_TYPE_CONFIG_REGISTER = 0x0001;
        internal const UInt16 PACKAGE_SETTINGS_TYPE_CONTROL_FSMC = 0x0002;
        internal const UInt16 PACKAGE_SETTINGS_TYPE_CONTROL_GPS = 0x0003;


        internal const UInt16 PACKAGE_SIZE_ADC = 1038;
        internal const UInt16 PACKAGE_MIN_SIZE_GPS = 8;

        internal const string CONFIG_FILE_NAME = "settings.conf";

        internal const string CONFIG_INIT_PORT = "COM1";
        internal const UInt32 CONFIG_INIT_BAUDRATE = 115200;
        internal const string CONFIG_INIT_CONTROLLER_IP = "192.168.1.8";
        internal const string CONFIG_INIT_CONTROLLER_MASK = "255.255.255.0";
        internal const UInt16 CONFIG_INIT_CONTROLLER_PORT = 55005;
        internal const string CONFIG_INIT_PC_IP = "192.168.1.2";
        internal const UInt16 CONFIG_INIT_PC_PORT = 55005;
    }
}
